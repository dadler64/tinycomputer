package com.tc

import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.io.IOException
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.util.*

class MonitorDevice {
    private var displayOut: OutputStreamWriter? = null
    private var monitorList: LinkedList<ActionListener>? = null

    constructor() {
        if (!TCSimulator.GRAPHICAL_MODE)
            this.displayOut = OutputStreamWriter(System.out)
        else
            this.monitorList = LinkedList()
    }

    constructor(paramOutputStream: OutputStream) {
        this.displayOut = OutputStreamWriter(paramOutputStream)
    }

    fun addActionListener(paramActionListener: ActionListener) {
        this.monitorList!!.add(paramActionListener)
    }

    fun ready(): Boolean {
        if (TCSimulator.GRAPHICAL_MODE)
            return true
        try {
            this.displayOut!!.flush()
            return true
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

        return false
    }

    fun reset() {
        if (TCSimulator.GRAPHICAL_MODE) {
            val localListIterator = this.monitorList!!.listIterator()
            while (localListIterator.hasNext()) {
                val localActionListener = localListIterator.next()
                localActionListener.actionPerformed(ActionEvent(1, 0, null))
            }
        }
    }

    fun write(paramChar: Char) {
        if (TCSimulator.GRAPHICAL_MODE) {
            val localListIterator = this.monitorList!!.listIterator()
            while (localListIterator.hasNext()) {
                val localActionListener = localListIterator.next()
                localActionListener.actionPerformed(ActionEvent(paramChar + "", 0, null))
            }
        }
        try {
            this.displayOut!!.write(paramChar.toInt())
            this.displayOut!!.flush()
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

    }
}