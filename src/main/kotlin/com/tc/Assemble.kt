package com.tc

import java.io.IOException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.NoSuchFileException

fun main(args: Array<String>) {
    val cmdHelp = "Usage: tc.TCasm [-warn] file.asm"
    var file = ""
    var warningFlag = false
    val localSymTab = SymTab()

//    tc.Scanner.warnings = ""

    for (i in args.indices) {
        if (args[i].isEmpty()) {
            error("Null arguments are not permitted.\n$cmdHelp")
        } else if (args[i][0] == '-') {
            if (args[i].equals("-warn", true)) {
                warningFlag = true
            } else {
                error("Unknown flag ('${args[i]}').\n$cmdHelp")
            }
        } else {
            file = args[i]
        }
    }

    if (file.isBlank()) {
        error("No .asm file specified.\n" + cmdHelp)
    }

    val filename = baseFilename(file)
    val instructionList = parseToInstruction(filename, warningFlag)
    passOne(localSymTab, instructionList)
    passTwo(localSymTab, instructionList)
    passThree(instructionList, filename)
    genSym(localSymTab, filename)

//    return tc.Scanner.warnings
}

fun passOne(paramSymTab: SymTab, paramList: List<Instruction>) {
    var i = -1

    for (localInstruction in paramList) {
        if (localInstruction.label.length > 20) {
            error("Labels can be no longer than 20 characters ('${localInstruction.label}').")
        }
        if (i > 65535) {
            error("Label cannot be represented in 16 bits ($i)")
        }
        if (!paramSymTab.insert(localInstruction.label, i)) {
            error("Duplicate label ('${localInstruction.label}').")
        }

        i = localInstruction.nextAddress(i)

        if (i == -2) {
            error("tc.Instruction not preceded by a .orig directive.")
        }
    }
}

fun passTwo(paramSymTab: SymTab, paramList: List<Instruction>) {
    var i = -1

    for (localInstruction in paramList) {
        val j = paramSymTab.lookup(localInstruction.labelRef)

        if (j < 0) {
            error("Symbol not found ('${localInstruction.labelRef}')")
        }
        if (localInstruction.op.op == 33) {
            localInstruction.offsetImmediate = j
        } else {
            localInstruction.offsetImmediate = j - (i + 1)
        }

        i = localInstruction.nextAddress(i)
    }
}

fun passThree(paramList: List<Instruction>, paramString: String) {
    val objFilename = "$paramString.obj"
    var localInstructionList = ArrayList<Int>()
    val path = FileSystems.getDefault().getPath(objFilename)
    val bufferedWriter = Files.newBufferedWriter(path)

    try {
        for (localInstruction in paramList) {
            if (localInstruction.op.op != 37) {
                localInstructionList = localInstruction.encode() as ArrayList<Int>
            }
        }
        for (localInt in localInstructionList) {
            bufferedWriter.write(localInt)
        }
        bufferedWriter.close()
    } catch (localIOException: IOException) {
        error("IOException: $localIOException%n");
    }
}

fun genSym(paramSymTab: SymTab, paramString: String) {
    val symFilename = "$paramString.sym"
    var localMutableSet = paramSymTab.labels
    val path = FileSystems.getDefault().getPath(symFilename)

    try {
        val bufferedWriter = Files.newBufferedWriter(path)

        bufferedWriter.write("// Symbol table\n")
        bufferedWriter.write("// Scope level 0:\n")
        bufferedWriter.write("//\tSymbol Name       Page Address\n")
        bufferedWriter.write("//\t----------------  ------------\n")

        for (item in localMutableSet) {
            val str2 = item
            bufferedWriter.write("//\t$str2")

            for (i in 0 until (16 - str2.length)) {
                bufferedWriter.write(" ")
            }

            val i = paramSymTab.lookup(str2)
            val str3 = formatAddress(i)
            bufferedWriter.write(" $str3\n")
        }
        bufferedWriter.newLine()
        bufferedWriter.close()
    } catch (localIOException: IOException) {
        error("IOException: $localIOException%n");
    }
}


/**
 * @return List containing all of the line of the given file
 */
fun parseToInstruction(paramString: String, paramBoolean: Boolean): List<Instruction> {
    val fileName = "$paramString.asm"
    val localInstructionList = ArrayList<Instruction>()
    var i = 1
    val path = FileSystems.getDefault().getPath(fileName)

    try {
        for (line in Files.readAllLines(path)) {
            val localInstruction = Instruction(line, i++, paramBoolean)
            if (localInstruction.valid) {
                localInstructionList.add(localInstruction)
            }
        }
        return localInstructionList
    } catch (localNoSuchFileException: NoSuchFileException) {
        error("Error: No such file ('$fileName')")
    }
}

/**
 * @return List containing all of the line of the given file
 */
fun parseToLines(paramString: String): List<String> {
    val fileName = "$paramString.asm"
    var i = 1
    val path = FileSystems.getDefault().getPath(fileName)

    try {
        return Files.readAllLines(path) as ArrayList<String>
    } catch (localNoSuchFileException: NoSuchFileException) {
        error("Error: No such file ('$fileName')")
    }
}

/**
 * Check for '.asm' extension at the end of the file and return filename without extension
 * @param paramString original filename with '.asm' extension
 * @return the name of the file without '.asm' at the end
 */
fun baseFilename(paramString: String): String {
    if (!paramString.endsWith(".asm", true)) {
        throw com.tc.AsmException("Input file must have .asm suffix ('$paramString')")
    }
    return paramString.substring(0, paramString.length - 4)
}

fun formatAddress(paramInt: Int): String {
    val str = "0000" + Integer.toHexString(paramInt).toUpperCase()
    return str.substring(str.length - 4)
}
