package com.tc

import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.WindowEvent
import java.awt.event.WindowListener
import java.io.File
import javax.swing.*
import javax.swing.event.TableModelEvent
import javax.swing.event.TableModelListener
import javax.swing.filechooser.FileFilter
import javax.swing.table.TableCellRenderer

class TC3GUI(private val paramMachine: Machine) : ActionListener, TableModelListener {
    val frame = JFrame("TC3 Simulator")

    private val fileChooser = JFileChooser(".")
    private val menuBar = JMenuBar()
    private val fileMenu = JMenu("File")
    private val aboutMenu = JMenu("About")
    private val openItem = JMenuItem("Open .obj File")
    private val quitItem = JMenuItem("Quit")
    private val commandItem = JMenuItem("Open tc.Command Output Window")
    private val versionItem = JMenuItem("Simulator Version")
    private val openActionCommand = "Open"
    private val quitActionCommand = "Quit"
    private val openCOWActionCommand = "OutputWindow"
    private val versionActionCommand = "Version"
    private val leftPanel = JPanel()


    private val controlPanel = JPanel()

    private val nextButton = JButton("Next")
    private val nextButtonCommand = "Next"
    private val stepButton = JButton("Step")
    private val stepButtonCommand = "Step"
    private val continueButton = JButton("Continue")
    private val continueButtonCommand = "Continue"
    private val stopButton = JButton("Stop")
    private val stopButtonCommand = "Stop"
    private val statusLabelRunning = "    Running "
    private val statusLabelSuspended = "Suspended "
    private val statusLabelHalted = "       Halted "
    private val statusLabel = JLabel("")
    private val runningColor = Color(43, 129, 51)
    private val suspendedColor = Color(209, 205, 93)
    private val haltedColor = Color(161, 37, 40)

    private val regTable: JTable

    private val commandPanel: CommandLinePanel

    private val commandOutputWindow: CommandOutputWindow

    private val memoryPanel = JPanel(BorderLayout())


    private val memTable: JTable

    private val memScrollPane: JScrollPane

    private val devicePanel = JPanel()
    private val registerPanel = JPanel()
    private val ioPanel: TextConsolePanel
    private val video: VideoConsole

    init {
        val localRegisterFile = paramMachine.registerFile
        this.regTable = JTable(localRegisterFile)
        var localTableColumn = this.regTable.columnModel.getColumn(0)
        localTableColumn.maxWidth = 30
        localTableColumn.minWidth = 30
        localTableColumn = this.regTable.columnModel.getColumn(2)
        localTableColumn.maxWidth = 30
        localTableColumn.minWidth = 30
        val localMemory = paramMachine.memory

        // tc.Memory Table
        this.memTable = object : JTable(localMemory) {
            private val serialVersionUID = 100L
            private val machine: Machine? = null

            override fun prepareRenderer(paramAnonymousTableCellRenderer: TableCellRenderer, paramAnonymousInt1: Int, paramAnonymousInt2: Int): Component {
                val localComponent = super.prepareRenderer(paramAnonymousTableCellRenderer, paramAnonymousInt1, paramAnonymousInt2)
                if (paramAnonymousInt2 == 0) {
                    val breakpointCheckBox = JCheckBox()
                    if (paramAnonymousInt1 < 65024) {
                        if (this@TC3GUI.paramMachine.isBreakPoint(paramAnonymousInt1)) {
                            breakpointCheckBox.isSelected = true
                            breakpointCheckBox.background = Color.red
                            breakpointCheckBox.foreground = Color.red
                        } else {
                            breakpointCheckBox.isSelected = false
                            breakpointCheckBox.background = background
                        }
                    } else {
                        breakpointCheckBox.isEnabled = false
                        breakpointCheckBox.background = Color.lightGray
                    }
                    return breakpointCheckBox
                }
                if (paramAnonymousInt1 == this@TC3GUI.paramMachine.pc) {
                    localComponent.background = Color.yellow
                } else {
                    localComponent.background = background
                }
                return localComponent
            }

            override fun tableChanged(paramAnonymousTableModelEvent: TableModelEvent) {
                try {
                    if (!this.machine!!.isContinueMode) {
                        super.tableChanged(paramAnonymousTableModelEvent)
                    }
                } catch (localNullPointerException: NullPointerException) {
                    super.tableChanged(paramAnonymousTableModelEvent)
                }

            }
        }
        this.memScrollPane = JScrollPane(this.memTable)

        // Width of 'Breakpoint Check Mark' column
        localTableColumn = this.memTable.getColumnModel().getColumn(0)
        localTableColumn.maxWidth = 21
        localTableColumn.minWidth = 21
        localTableColumn.cellEditor = DefaultCellEditor(JCheckBox())

        // Width of 'Value' column
        localTableColumn = this.memTable.getColumnModel().getColumn(2)
        localTableColumn.minWidth = 50
        localTableColumn.maxWidth = 50

        this.commandPanel = CommandLinePanel(paramMachine)

        this.commandOutputWindow = CommandOutputWindow("tc.Command Output")

        val local2 = object : WindowListener {
            override fun windowActivated(paramAnonymousWindowEvent: WindowEvent) {}

            override fun windowClosed(paramAnonymousWindowEvent: WindowEvent) {}

            override fun windowClosing(paramAnonymousWindowEvent: WindowEvent) {
                this@TC3GUI.commandOutputWindow.isVisible = false
            }

            override fun windowDeactivated(paramAnonymousWindowEvent: WindowEvent) {}

            override fun windowDeiconified(paramAnonymousWindowEvent: WindowEvent) {}

            override fun windowIconified(paramAnonymousWindowEvent: WindowEvent) {}

            override fun windowOpened(paramAnonymousWindowEvent: WindowEvent) {}
        }

        this.commandOutputWindow.addWindowListener(local2)
        this.commandOutputWindow.setSize(700, 600)

        this.ioPanel = TextConsolePanel(paramMachine.keyBoardDevice, paramMachine.monitorDevice)
        this.ioPanel.minimumSize = Dimension(256, 85)
        this.video = VideoConsole(paramMachine)
        this.commandPanel.setGUI(this)
    }

    // Initiaize tc.Memory panel
    private fun setupMemoryPanel() {
        this.memoryPanel.add(this.memScrollPane, "Center")

        this.memoryPanel.minimumSize = Dimension(400, 100)
        this.memoryPanel.border = BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("tc.Memory"), BorderFactory.createEmptyBorder(5, 5, 5, 5))

        this.memTable.model.addTableModelListener(this)
        this.memTable.model.addTableModelListener(this.video)

        this.memTable.preferredScrollableViewportSize = Dimension(400, 460)
    }

    private fun setupControlPanel() {
        val i = 4
        this.controlPanel.layout = GridBagLayout()
        var localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.fill = 2

        this.nextButton.actionCommand = "Next"
        this.nextButton.addActionListener(this)
        localGridBagConstraints.weightx = 1.0
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 0
        this.controlPanel.add(this.nextButton, localGridBagConstraints)

        this.stepButton.actionCommand = "Step"
        this.stepButton.addActionListener(this)
        localGridBagConstraints.gridx = 1
        localGridBagConstraints.gridy = 0
        this.controlPanel.add(this.stepButton, localGridBagConstraints)

        this.continueButton.actionCommand = "Continue"
        this.continueButton.addActionListener(this)
        localGridBagConstraints.gridx = 2
        localGridBagConstraints.gridy = 0
        this.controlPanel.add(this.continueButton, localGridBagConstraints)

        this.stopButton.actionCommand = "Stop"
        this.stopButton.addActionListener(this)
        localGridBagConstraints.gridx = 3
        localGridBagConstraints.gridy = 0
        this.controlPanel.add(this.stopButton, localGridBagConstraints)

        localGridBagConstraints.gridx = 4
        localGridBagConstraints.gridy = 0
        localGridBagConstraints.fill = 0
        localGridBagConstraints.anchor = 22
        setStatusLabelSuspended()
        this.controlPanel.add(this.statusLabel, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 1
        localGridBagConstraints.gridwidth = 6
        this.controlPanel.add(Box.createRigidArea(Dimension(5, 5)), localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 2
        localGridBagConstraints.gridwidth = 6
        localGridBagConstraints.gridheight = 1
        localGridBagConstraints.ipady = 100
        localGridBagConstraints.weightx = 1.0
        localGridBagConstraints.weighty = 1.0
        localGridBagConstraints.fill = 1
        this.controlPanel.add(this.commandPanel, localGridBagConstraints)

        this.controlPanel.minimumSize = Dimension(100, 150)
        this.controlPanel.preferredSize = Dimension(100, 150)
        this.controlPanel.border = BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Controls"), BorderFactory.createEmptyBorder(5, 5, 5, 5))

        this.controlPanel.isVisible = true
    }

    private fun setupRegisterPanel() {
        this.registerPanel.layout = GridBagLayout()
        val localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 0
        localGridBagConstraints.weightx = 1.0
        localGridBagConstraints.fill = 2
        this.registerPanel.add(this.regTable, localGridBagConstraints)

        this.registerPanel.border = BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Registers"), BorderFactory.createEmptyBorder(5, 5, 5, 5))

        this.registerPanel.isVisible = true
    }

    private fun setupDevicePanel() {
        this.devicePanel.layout = GridBagLayout()
        var localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.fill = 10

        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 0
        localGridBagConstraints.weightx = 1.0
        this.devicePanel.add(this.video, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 1
        localGridBagConstraints.weightx = 1.0
        localGridBagConstraints.fill = 0
        this.devicePanel.add(this.ioPanel, localGridBagConstraints)
        this.devicePanel.border = BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Devices"), BorderFactory.createEmptyBorder(5, 5, 5, 5))

        this.devicePanel.isVisible = true
    }

    fun setUpGUI() {
        initLookAndFeel()

        JFrame.setDefaultLookAndFeelDecorated(true)

        this.paramMachine.setStoppedListener(this.commandPanel)

        this.fileChooser.fileSelectionMode = 2
        this.fileChooser.addChoosableFileFilter(object : FileFilter() {

            override fun accept(paramAnonymousFile: File): Boolean {
                if (paramAnonymousFile.isDirectory)
                    return true
                val str = paramAnonymousFile.name
                return str != null && str.toLowerCase().endsWith(".obj")
            }

            override fun getDescription(): String {
                return "*.obj"
            }
        })

        this.openItem.actionCommand = "Open"
        this.openItem.addActionListener(this)
        this.fileMenu.add(this.openItem)
        this.commandItem.actionCommand = "OutputWindow"
        this.commandItem.addActionListener(this)
        this.fileMenu.add(this.commandItem)
        this.fileMenu.addSeparator()
        this.quitItem.actionCommand = "Quit"
        this.quitItem.addActionListener(this)
        this.fileMenu.add(this.quitItem)

        this.versionItem.actionCommand = "Version"
        this.versionItem.addActionListener(this)
        this.aboutMenu.add(this.versionItem)

        this.menuBar.add(this.fileMenu)
        this.menuBar.add(this.aboutMenu)
        this.frame.jMenuBar = this.menuBar

        setupControlPanel()
        setupRegisterPanel()
        setupDevicePanel()
        setupMemoryPanel()

        this.regTable.model.addTableModelListener(this)

        this.frame.contentPane.layout = GridBagLayout()
        var localGridBagConstraints = GridBagConstraints()

        localGridBagConstraints.fill = 1
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 0
        localGridBagConstraints.gridwidth = 2
        localGridBagConstraints.weighty = 1.0

        localGridBagConstraints.gridwidth = 0
        this.frame.contentPane.add(this.controlPanel, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 1

        localGridBagConstraints.gridwidth = 1
        localGridBagConstraints.gridheight = 1
        localGridBagConstraints.weightx = 0.0
        localGridBagConstraints.fill = 2
        this.frame.contentPane.add(this.registerPanel, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 0
        localGridBagConstraints.gridy = 2
        localGridBagConstraints.weightx = 0.0

        localGridBagConstraints.gridheight = 1
        localGridBagConstraints.gridwidth = 1

        localGridBagConstraints.fill = 1
        this.frame.contentPane.add(this.devicePanel, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridx = 1
        localGridBagConstraints.gridy = 1

        localGridBagConstraints.gridheight = 2
        localGridBagConstraints.gridwidth = 0
        localGridBagConstraints.fill = 1
        localGridBagConstraints.weightx = 1.0
        this.frame.contentPane.add(this.memoryPanel, localGridBagConstraints)

        this.frame.size = Dimension(700, 725)
        this.frame.defaultCloseOperation = 3
        this.frame.pack()
        this.frame.isVisible = true

        scrollToPC()
    }

    fun clearCommandLine() {
        this.commandPanel.clear()
    }

    fun scrollToIndex(paramInt: Int) {
        this.memTable.scrollRectToVisible(this.memTable.getCellRect(paramInt, 0, true))
    }

    @JvmOverloads
    fun scrollToPC(paramInt: Int = 0) {
        val i = this.paramMachine.registerFile.pc.getValue() + paramInt
        this.memTable.scrollRectToVisible(this.memTable.getCellRect(i, 0, true))
    }

    override fun tableChanged(paramTableModelEvent: TableModelEvent) {
        if (!this.paramMachine.isContinueMode) {

        }
    }

    fun confirmExit() {
        val arrayOfObject = arrayOf("Yes", "No")
        val i = JOptionPane.showOptionDialog(this.frame, "Are you sure you want to quit?", "Quit verification", 0, 3, null, arrayOfObject, arrayOfObject[1])

        if (i == 0) {
            ErrorLog.logClose()
            System.exit(0)
        }
    }

    override fun actionPerformed(paramActionEvent: ActionEvent) {
        try {
            if ("Next" == paramActionEvent.actionCommand)
                this.paramMachine.executeNext()
            else if ("Step" == paramActionEvent.actionCommand)
                this.paramMachine.executeStep()
            else if ("Continue" == paramActionEvent.actionCommand)
                this.paramMachine.executeMany()
            else if ("Quit" == paramActionEvent.actionCommand)
                confirmExit()
            else if ("Stop" == paramActionEvent.actionCommand)
                CommandLinePanel.writeToConsole(this.paramMachine.stopExecution(true))
            else if ("OutputWindow" == paramActionEvent.actionCommand)
                this.commandOutputWindow.isVisible = true
            else if ("Version" == paramActionEvent.actionCommand)
                JOptionPane.showMessageDialog(this.frame, TCSimulator.getVersion(), "Version", 1)
            else if ("Open" == paramActionEvent.actionCommand) {
                val i = this.fileChooser.showOpenDialog(this.frame)
                if (i == 0) {
                    val localFile = this.fileChooser.selectedFile
                    CommandLinePanel.writeToConsole(this.paramMachine.loadObjectFile(localFile))
                } else
                    println("Open command cancelled by user.")
            }
        } catch (localCustomException: CustomException) {
            localCustomException.showMessageDialog(this.frame)
        }

    }

    fun setStatusLabelRunning() {
        this.statusLabel.text = "    Running "
        this.statusLabel.foreground = this.runningColor
    }

    fun setStatusLabelSuspended() {
        this.statusLabel.text = "Suspended "
        this.statusLabel.foreground = this.suspendedColor
    }

    fun setStatusLabelHalted() {
        this.statusLabel.text = "       Halted "
        this.statusLabel.foreground = this.haltedColor
    }

    fun setStatusLabel(paramBoolean: Boolean) {
        if (paramBoolean) {
            setStatusLabelSuspended()
        } else {
            setStatusLabelHalted()
        }
    }

    fun setTextConsoleEnabled(paramBoolean: Boolean) {
        this.ioPanel.isEnabled = paramBoolean
    }

    companion object {
//        var LOOKANDFEEL: String = "Metal"
//        var LOOKANDFEEL: String = "GTK+"
//        var LOOKANDFEEL: String = "System"
//        var LOOKANDFEEL: String = "Motif"
//        var LOOKANDFEEL: String = "Windows"
//        var LOOKANDFEEL: String = "Nimbus"
        var LOOKANDFEEL: String = "Nimbus"



        // TODO: Learn how themeing works and add options to menu bar
        fun initLookAndFeel() {
            val str: String
            JFrame.setDefaultLookAndFeelDecorated(true)
            if (LOOKANDFEEL.isNotEmpty()) {
                str = when (LOOKANDFEEL) {
                    "Metal" -> UIManager.getCrossPlatformLookAndFeelClassName()
                    "System" -> UIManager.getSystemLookAndFeelClassName()
                    "Motif" -> "com.sun.java.swing.plaf.motif.MotifLookAndFeel"
                    "GTK+" -> "com.sun.java.swing.plaf.gtk.GTKLookAndFeel"
                    "Windows" -> "com.sun.java.swing.plaf.windows.WindowsLookAndFeel"
                    "Nimbus" -> "com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel"
                    else -> {
                        ErrorLog.logError("Unexpected value of LOOKANDFEEL specified: $LOOKANDFEEL")
                        UIManager.getCrossPlatformLookAndFeelClassName()
                    }
                }
                try {
                    UIManager.setLookAndFeel(str)
                } catch (localClassNotFoundException: ClassNotFoundException) {
                    ErrorLog.logError("Couldn't find class for specified look and feel:" + str)
                    ErrorLog.logError("Did you include the L&F library in the class path?")
                    ErrorLog.logError("Using the default look and feel.")
                } catch (localUnsupportedLookAndFeelException: UnsupportedLookAndFeelException) {
                    ErrorLog.logError("Can't use the specified look and feel ($str) on this platform.")
                    ErrorLog.logError("Using the default look and feel.")
                } catch (localException: Exception) {
                    ErrorLog.logError("Couldn't get specified look and feel ($str), for some reason.")
                    ErrorLog.logError("Using the default look and feel.")
                    ErrorLog.logError(localException)
                }

            }
        }
    }
}