package com.tc

/**
 * Lexeme - a basic lexical unit of a language, consisting of one word or several words, considered as
 *          an abstract unit, and applied to a family of words related by form or meaning.
 */
class Scanner @Throws(com.tc.AsmException::class)
constructor(paramString: String, paramInt: Int, var warn: Boolean) {
    var line: String
    var scanning = true
    var pos = -1
    var lineno = -1

    init {
        this.line = expand_tabs(paramString, 8)
        this.pos = 0
        this.lineno = paramInt
        remainingLine
    }

    val remainingLine: String
        @Throws(com.tc.AsmException::class)
        get() {
            checkValidLine()
            val str1 = this.line!!.substring(this.pos)
            val str2 = skipws(str1)
            this.pos += str1.length - str2.length
            return str2
        }

    @Throws(com.tc.AsmException::class)
    fun getLexemePnum(paramInt1: Int, paramInt2: Int): String {
        checkValidLine()
        val str = remainingLine.substring(paramInt2)

        var i = 0
        while (i < str.length) {
            var char = str[i]
            if (paramInt1 == 2)
                if (char != '0' && char != '1')
                    break
                else if (paramInt1 == 10)
                    if (!Character.isDigit(char))
                        break
                    else if (paramInt1 == 16) {
                        char = Character.toLowerCase(char)
                        if (!Character.isDigit(char) && char != 'a' && char != 'b' && char != 'c' && char != 'd' && char != 'e' && char != 'f')
                            break
                    } else
                        internalError("Illegal base ($paramInt1)")
            i++
        }
        return str.substring(0, i)
    }

    @Throws(com.tc.AsmException::class)
    fun getLexemeNum(paramInt1: Int, paramInt2: Int): String {
        checkValidLine()

        val str1 = remainingLine.substring(paramInt2)

        if (str1.isEmpty())
            return ""

        if (str1[0] == '-') {
            val str2 = getLexemePnum(paramInt1, paramInt2 + 1)
            return if (str2.isNotEmpty()) "-" + str2 else str2
        }
        return getLexemePnum(paramInt1, paramInt2)
    }

    val lexemeImmediate: String
        @Throws(com.tc.AsmException::class)
        get() {
            checkValidLine()
            val str2 = remainingLine
            if (str2.isEmpty())
                return str2
            val str1: String

            when (str2[0]) {
                '#' -> {
                    str1 = getLexemeNum(10, 1)
                    return if (str1.isEmpty()) str1 else "#" + str1
                }
                'x' -> {
                    str1 = getLexemeNum(16, 1)
                    return if (str1.isEmpty()) str1 else "x" + str1
                }
                'b' -> {
                    str1 = getLexemeNum(2, 1)
                    return if (str1.isEmpty()) str1 else "b" + str1
                }
            }

            return if (Character.isDigit(str2[0]) || str2[0] == '-') getLexemeNum(10, 0) else ""
        }

    val lexeme_reg: String
        @Throws(com.tc.AsmException::class)
        get() {
            checkValidLine()
            val str1 = remainingLine

            if (str1.length < 2)
                return ""
            val i = str1[0].toInt()
            if (i != 114 && i != 82)
                return ""

            val str2 = getLexemeNum(10, 1)
            return if (str2.length == 0) str2 else str1.substring(0, str2.length + 1)
        }

    val string: String?
        @Throws(com.tc.AsmException::class)
        get() {
            checkValidLine()
            val str1 = remainingLine
            var str2 = ""
            if (str1.length == 0)
                return null

            for (i in 0..str1.length - 1) {
                str2 = str2 + str1[i]
                if (i == 0 && str1[i] != '"')
                    return null
                if (i != 0 && str1[i] == '"')
                    break
            }
            return str2
        }

    fun get_real_string(paramString: String?): String? {
        if (paramString == null || paramString.length < 2)
            return null
        if (paramString[0] != '"' || paramString[paramString.length - 1] != '"')
            return null
        val str1 = paramString.substring(1, paramString.length - 1)
        var str2 = ""
        var i = 0
        while (i < str1.length) {
            if (str1[i] == '\\') {
                if (i == str1.length - 1) return null
                val j = str1[++i].toInt()
                if (j == 110)
                    str2 = str2 + '\n'
                else
                    return null
            } else
                str2 = str2 + str1[i]
            i++
        }
        return str2
    }

    @Throws(com.tc.AsmException::class)
    fun get_lexeme_lab(paramBoolean: Boolean): String {
        checkValidLine()
        val str = remainingLine

        if (str.isEmpty())
            return str

        var i = 0
        while (i < str.length) {
            val c = str[i]
            val j = if (Character.isLetter(c) || c == '_' || paramBoolean && c == '.' || Character.isDigit(c) && i != 0) 1 else 0
            if (j == 0)
                break
            i++
        }
        return str.substring(0, i)
    }

    @Throws(com.tc.AsmException::class)
    fun eat(paramString: String?): Boolean {
        checkValidLine()
        if (paramString == null)
            return false
        val str1 = remainingLine
        val str2 = skipws(paramString)

        if (!str1.startsWith(str2))
            return false
        this.pos += str2.length
        return true
    }

    // TODO Use JDK tc.Scanner class instead
    @Throws(com.tc.AsmException::class)
    private fun checkValidLine() {
        if (this.line.isBlank()) {
            internalError("Invalid tc.Scanner object.")
        }
        else if (this.pos == -1) {
            internalError("Invalid position in tc.Scanner object.")
        }
        else if (this.lineno == -1) {
            internalError("Invalid line number in tc.Scanner object.")
        }
        else if (this.pos > this.line.length)
            internalError("Invalid position in line (${this.pos}/${this.line.length}).")
    }

    @Throws(com.tc.AsmException::class)
    fun imm_val(paramString: String): Int {
        val str = skipws(paramString)
        var i = 0
        var j = 1

        if (str == null || str.isEmpty())
            internalError("Bad immediate ('$str')")

        val c = str[0]
        if (c == 'x')
            i = 16
        else if (c == '#')
            i = 10
        else if (Character.isDigit(c) || c == '-') {
            i = 10
            j = 0
        } else if (c == 'b')
            i = 2
        else
            internalError("No prefix on immediate ('$str')")

        return Integer.parseInt(str.substring(j), i)
    }

    private val currentString: String
        @Throws(com.tc.AsmException::class)
        get() {
            checkValidLine()
            return this.line.substring(this.pos)
        }

    fun printOutLine(): String {
        var str = ""
            str = str + this.line + "\n"
            if (this.scanning) {
                for (i in 0 until this.pos)
                    str += " "
                str += "^\n"
            }
        return str
    }

    fun warning(paramString: String) {
        if (this.warn) {
            warnings += "WARNING: (line ${this.lineno}): $paramString\n"
        }
    }

    @Throws(com.tc.AsmException::class)
    fun error(paramString: String) {
        var str = "ERROR (line " + this.lineno + "): " + paramString + "\n"
        str += printOutLine()
        throw com.tc.AsmException(str)
    }


    @Throws(com.tc.AsmException::class)
    fun internalError(paramString: String) {
        var str = "INTERNAL ERROR: " + paramString + "\n"
        str += "Please report to cse240@seas.upenn.edu.\n"
        str += "Include the .asm file that produced this error.\n"
        str += printOutLine()
        throw com.tc.AsmException(str)
    }

    companion object {
        var warnings = ""

        fun regNum(paramString: String): Int {
            val str = skipws(paramString)

            return if (str.length < 2 || Character.toLowerCase(str[0]) != 'r') -1 else Integer.parseInt(str.substring(1))

        }

        fun getOpcodeFromLexeme(paramString: String): Ops? {
            val str = skipws(paramString)
            return if (str.isEmpty()) null else Ops.buildOps(str)

        }

        private fun expand_tabs(paramString: String, paramInt: Int): String {
            val localStringBuffer = StringBuffer()
            var i = 0

            for (j in 0 until paramString.length) {
                val c = paramString[j]
                if (c == '\t') {
                    val k = paramInt - i % paramInt
                    for (m in 0 until k)
                        localStringBuffer.append(' ')
                    i += k
                } else {
                    localStringBuffer.append(c)
                    if (c == '\n')
                        i = 0
                    else
                        i++
                }
            }
            return localStringBuffer.toString()
        }

        fun skipws(paramString: String): String {
            for (i in 0 until paramString.length) {
                val j = paramString[i].toInt()
                if (j != 32 && j != 9 && j != 10)
                    return paramString.substring(i)
            }
            return ""
        }
    }
}