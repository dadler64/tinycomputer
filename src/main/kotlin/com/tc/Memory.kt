package com.tc

class Memory(private val machine: Machine) : TC3TableModel() {
    var kbDevice = KeyboardDevice()
    var monitorDevice = MonitorDevice()
    var timerDevice = TimerDevice()
    private val memArr = arrayOfNulls<Word>(65535)
    private val colNames = arrayOf("BP", "Address", "Value", "tc.Instruction")
    private val breakPoints = BooleanArray(65535)

    init {
        for (i in 0..65534) {
            this.memArr[i] = Word()
            this.breakPoints[i] = false
        }
        this.timerDevice.setTimer()
    }

    fun reset() {
        for (i in 0..65534)
            this.memArr[i]?.reset()
        this.kbDevice.reset()
        this.monitorDevice.reset()
        this.timerDevice.reset()
        clearAllBreakPoints()

        fireTableRowsUpdated(0, 65534)
    }

    override fun getRowCount(): Int {
        return this.memArr.size
    }

    override fun getColumnCount(): Int {
        return this.colNames.size
    }

    override fun getColumnName(paramInt: Int): String {
        return this.colNames[paramInt]
    }

    override fun isCellEditable(paramInt1: Int, paramInt2: Int): Boolean {
        return (paramInt2 == 2 || paramInt2 == 0) && paramInt1 < 65024
    }


    fun symbolExists(paramString: String): Boolean {
        return this.machine.lookupSym(paramString) != Integer.MAX_VALUE
    }

    fun getSymbolAddress(paramString: String): Int {
        return this.machine.lookupSym(paramString)
    }

    fun breakPointSet(paramInt: Int): Boolean {
        return this.breakPoints[paramInt]
    }

    fun setBreakPoint(paramString: String): String {
        val i = this.machine.getAddress(paramString)
        var str: String
        if (i != Integer.MAX_VALUE) {
            str = setBreakPoint(i)
            if (symbolExists(paramString))
                str = "$str ('$paramString')"
        } else
            str = "Error: Invalid address or label ('$paramString')"
        return str
    }

    fun setBreakPoint(paramInt: Int): String {
        if (paramInt < 0 || paramInt >= 65535)
            return "Error: Invalid address or label"
        this.breakPoints[paramInt] = true
        fireTableRowsUpdated(paramInt, paramInt)
        return "Breakpoint set at " + Word.toHex(paramInt)
    }

    fun clearBreakPoint(paramString: String): String {
        val i = this.machine.getAddress(paramString)
        var str: String
        if (i != Integer.MAX_VALUE) {
            str = clearBreakPoint(i)
            if (symbolExists(paramString))
                str = "$str ('$paramString')"
        } else
            str = "Error: Invalid address or label ('$paramString')"
        return str
    }

    fun clearBreakPoint(paramInt: Int): String {
        if (paramInt < 0 || paramInt >= 65535)
            return "Error: Invalid address or label"
        this.breakPoints[paramInt] = false
        fireTableRowsUpdated(paramInt, paramInt)
        return "Breakpoint cleared at " + Word.toHex(paramInt)
    }

    fun clearAllBreakPoints() {
        for (i in 0..65534)
            this.breakPoints[i] = false
    }

    override fun getValueAt(paramInt1: Int, paramInt2: Int): Any? {
        var localObject: Any? = paramInt1.toString()
        when (paramInt2) {


            0 -> localObject = breakPointSet(paramInt1)

            1 -> {
                localObject = Word.toHex(paramInt1)
                val str = this.machine.lookupSym(paramInt1)
                if (str != null) {
                    localObject = localObject.toString() + " " + str
                }
            }
            2 -> if (paramInt1 < 65024) {
                localObject = this.memArr[paramInt1]?.toHex()
            } else {
                localObject = "???"
            }

            3 -> if (paramInt1 < 65024) {
                localObject = Instruction.disassemble(this.memArr[paramInt1]!!, paramInt1, this.machine)
            } else {
                localObject = "Use 'list' to query"
            }
        }

        return localObject
    }


    fun getInst(paramInt: Int): Word {
        return this.memArr[paramInt]!!
    }


    fun read(paramInt: Int): Word? {
        var localWord: Word? = null
        when (paramInt) {
            65024 -> if (this.kbDevice.available())
                localWord = Machine.KB_AVAILABLE
            else
                localWord = Machine.KB_UNAVAILABLE
            65026 -> localWord = this.kbDevice.read()
            65028 -> if (this.monitorDevice.ready())
                localWord = Machine.MONITOR_READY
            else
                localWord = Machine.MONITOR_NOTREADY
            65032 -> if (this.timerDevice.hasGoneOff())
                localWord = Machine.TIMER_SET
            else
                localWord = Machine.TIMER_UNSET
            65034 -> localWord = Word(this.timerDevice.interval.toInt())
            65042 -> localWord = Word(this.machine.mpr)
            65534 -> localWord = Word(this.machine.mcr)
            else -> {
                if (paramInt < 0 || paramInt >= 65535)
                    return null
                localWord = this.memArr[paramInt]
            }
        }
        return localWord
    }


    override fun setValueAt(paramObject: Any?, paramInt1: Int, paramInt2: Int) {
        if (paramInt2 == 2)
            write(paramInt1, Word.parseNum(paramObject.toString()))
        if (paramInt2 == 0) {
            if ((paramObject as Boolean?)!!)
                CommandLinePanel.writeToConsole(setBreakPoint(paramInt1))
            else
                CommandLinePanel.writeToConsole(clearBreakPoint(paramInt1))
        }
        fireTableRowsUpdated(paramInt1, paramInt1)
    }

    fun write(paramInt1: Int, paramInt2: Int) {
        when (paramInt1) {
            65030 -> {
                this.monitorDevice.write(paramInt2.toChar())
                fireTableCellUpdated(paramInt1, 3)
            }
            65034 -> {
                this.timerDevice.setTimer(paramInt2.toLong())
                if (paramInt2 == 0)
                    this.timerDevice.isEnabled = false
                else {
                    this.timerDevice.isEnabled = true
                    if (paramInt2 == 1)
                        this.timerDevice.setTimer(this.kbDevice)
                }
            }
            65042 -> this.machine.mpr = paramInt2
            65534 -> {
                this.machine.mcr = paramInt2
                if (paramInt2 and 0x8000 == 0)
                    this.machine.stopExecution(1, true)
                else
                    this.machine.updateStatusLabel()
            }
        }
        this.memArr[paramInt1]?.setValue(paramInt2)
        fireTableRowsUpdated(paramInt1, paramInt1)
    }

    companion object {
        val BEGIN_DEVICE_REGISTERS = 65024
        val KBSR = 65024
        val KBDR = 65026
        val DSR = 65028
        val DDR = 65030
        val TMR = 65032
        val TMI = 65034
        val DISABLE_TIMER = 0
        val MANUAL_TIMER_MODE = 1
        val MPR = 65042
        val MCR = 65534
        val breakPointColumn = 0
        val addressColumn = 1
        val valueColumn = 2
        val insnColumn = 3
        private val serialVersionUID = 102L
    }
}