package com.tc

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Any?) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Int) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Long) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Byte) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Short) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Char) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Boolean) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Float) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: Double) {
    System.err.print(message)
}

/** Prints the given message to the standard output stream. */
public inline fun printerr(message: CharArray) {
    System.err.print(message)
}