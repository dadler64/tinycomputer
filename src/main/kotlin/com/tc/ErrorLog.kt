package com.tc

import java.io.BufferedWriter
import java.io.IOException
import java.nio.file.FileSystems
import java.nio.file.Files
import java.util.*

object ErrorLog {

    private lateinit var log: BufferedWriter
    private val logDelim = "\n-----\n"
    private val logOpen = false

    /**
     * Initialize a new log file
     */
    fun logInit() {
        if (!logOpen) {
            log = try {
                Files.newBufferedWriter(FileSystems.getDefault().getPath("TC.log"))
            } catch (localIOException: IOException) {
                error("Failed to create a log file!")
            }
            debug("Log opened!")
        } else if (logOpen) {
            debug("Log is already open!")
        } else {
            warning("Unknown log state!")
        }

    }


    /**
     * Write timestamp to log
     */
    private fun logTimeStamp() {
        if (!logOpen)
            logInit()
        log.write("[ ${Date(Calendar.getInstance().timeInMillis)} ]: ")
    }

    /**
     * Write string to log
     */
    fun logError(paramString: String) {
        if (!logOpen)
            logInit()
        logTimeStamp()
        log.write(paramString)
        log.write(logDelim)
    }

    /**
     * Write exception to log
     */
    fun logError(paramException: Exception) {
        if (!logOpen)
            logInit()
        logTimeStamp()
        log.write(paramException.printStackTrace().toString())
        log.write(logDelim)
    }

    /**
     * Write timestamp to log
     */
    fun logClose() {
        if (!logOpen) {
            info("logClose() called when log was never opened!")
        log.close()
            info("Log closed!")
        }
    }
}