package com.tc

import java.awt.Color
import kotlin.experimental.and

class Word {
    private var value: Int = 0

    constructor(paramInt: Int) {
        this.value = paramInt and 0xFFFF
    }

    constructor() {
        this.value = 0
    }

    /**
     * reset value
     */
    fun reset() {
        this.value = 0
    }

    fun toHex(): String? {
        return toHex(this.value)
    }

    override fun toString(): String {
        return "" + this.value
    }

    fun getValue(): Int {
        return this.value
    }

    fun setValue(paramInt: Int) {
        this.value = paramInt and 0xFFFF
    }

    fun convertToRGB(): Int {
        return Color(getZext(14, 10) * 8, getZext(9, 5) * 8, getZext(4, 0) * 8).rgb
    }

    fun getZext(paramInt1: Int, paramInt2: Int): Int {
        var i = this.value

        if (paramInt2 > paramInt1) {
            return getZext(paramInt2, paramInt1)
        }

        i = -1 shl paramInt1 + 1 xor 0xFFFFFFFF.toInt() and i
        i = i shr paramInt2
        return i
    }


    fun getSext(paramInt1: Int, paramInt2: Int): Int {
        var i = this.value

        // If paramInt2 is greater than paramInt1 rerun the function with the parameters swapped
        if (paramInt2 > paramInt1) {
            return getSext(paramInt2, paramInt1)
        }

        val j = i and (1 shl paramInt1)
        i = if (j != 0) {
            -1 shl paramInt1 or i
        } else {
            (-1).inv() shl paramInt1 + 1 and i
        }

        i = i shr paramInt2
        return i
    }

    fun getBit(paramInt: Int): Int {
        return getZext(paramInt, paramInt)
    }

    companion object {

        fun toHex(paramInt: Int): String? {
            if (paramInt < 32768 || paramInt > 65535) {
                return null
            }

            val str = StringBuilder("x")
            val arrayOfInt = intArrayOf(15, 240, 3840, 61440)
            val arrayOfChar = charArrayOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F')

            for (i in 3 downTo 0) {
                var j = paramInt and arrayOfInt[i]
                j = j shr 4 * i
                str.append(arrayOfChar[j])
            }
            return str.toString()
        }

        fun parseNum(paramString: String): Int {
            val i: Int = try {
                if (paramString.indexOf('x') == 0) {
                    Integer.parseInt(paramString.replace('x', '0'), 16)
                } else {
                    Integer.parseInt(paramString)
                }
            } catch (localNumberFormatException: NumberFormatException) {
                Integer.MAX_VALUE
            }

            return i
        }

        fun convertByteArray(paramByte1: Byte, paramByte2: Byte): Int {
            var i = 0
            val b = 255.toByte()
            i += b and paramByte1
            i *= 256
            i += b and paramByte2
            return i
        }
    }
}
