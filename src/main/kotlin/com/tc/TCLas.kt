package com.tc

object TCLas {
    @JvmStatic
    fun main(args: Array<String?>) {
        val localLC3as = TCasm()
        val str: String

        try {
            str = localLC3as.asm(args)
        } catch (localAsmException: com.tc.AsmException) {
            error("Error: " + localAsmException.message)
        }
    }
}