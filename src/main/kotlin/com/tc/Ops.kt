package com.tc

class Ops internal constructor(val op: Int) {

    val opstring: String
    val opcode: Int

    init {
        this.opstring = opstrings[op]
        this.opcode = opcodes[op]
    }

    companion object {
        internal val NULL = -1

        internal val ADD = 0
        internal val AND = 1
        internal val MUL = 2
        internal val BRNZP = 3
        internal val BRNZ = 4
        internal val BRNP = 5
        internal val BRN = 6
        internal val BRZP = 7
        internal val BRZ = 8
        internal val BRP = 9
        internal val BR = 10
        internal val LEA = 11
        internal val LDR = 12
        internal val STR = 13
        internal val LDI = 14
        internal val STI = 15
        internal val LD = 16
        internal val ST = 17
        internal val RTI = 18
        internal val JSRR = 19
        internal val JSR = 20
        internal val NOT = 21
        internal val RET = 22
        internal val RTT = 23
        internal val JMP = 24
        internal val JMPT = 25
        internal val TRAP = 26
        internal val GETC = 27
        internal val OUT = 28
        internal val PUTS = 29
        internal val IN = 30
        internal val PUTSP = 31
        internal val HALT = 32
        internal val FILL = 33
        internal val BLKW = 34
        internal val ORIG = 35
        internal val STRINGZ = 36
        internal val END = 37
        internal val LAST = 38
        internal val opstrings = arrayOf("add", "and", "mul", "brnzp", "brnz", "brnp", "brn", "brzp", "brz", "brp", "br", "lea", "ldr", "str", "ldi", "sti", "ld", "st", "rti", "jsrr", "jsr", "not", "ret", "rtt", "jmp", "jmpt", "trap", "getc", "out", "puts", "in", "putsp", "halt", ".fill", ".blkw", ".orig", ".stringz", ".end")
        internal val opcodes = intArrayOf(1, 5, 13, 0, 0, 0, 0, 0, 0, 0, 0, 14, 6, 7, 10, 11, 2, 3, 8, 4, 4, 9, 12, 12, 12, 12, 15, 15, 15, 15, 15, 15, 15, -1, -1, -1, -1, -1)

        internal fun buildOps(paramString: String): Ops {
            for (i in 0..37) {
                if (paramString.equals(opstrings[i], ignoreCase = true))
                    return Ops(i)
            }
            return Ops(-1)
        }
    }
}





