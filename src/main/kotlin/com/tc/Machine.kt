package com.tc

import java.awt.event.ActionListener
import java.io.*
import java.util.*
import javax.swing.SwingUtilities

class Machine : Runnable {
    private val symbolTable = Hashtable<String, Int>()
    private val inverseTable = Hashtable<Int, String>()
    var CHECKS_PASSED = 0
    var CHECKS_FAILED = 0
    internal var str1 = ""
    internal var stopImmediately = false
    val memory: Memory
    val registerFile: RegisterFile
    private var gui: TC3GUI? = null
    private val NotifyOnStop: LinkedList<ActionListener>
    private val nextBreakPoints = HashSet<Int>(10)
    var isContinueMode = false
        private set
    val monitorDevice: MonitorDevice
        get() = this.memory.monitorDevice
    var pc: Int
        get() = this.registerFile.pc.getValue()
        set(paramInt) {
            val i = this.registerFile.pc.getValue()
            this.registerFile.setPC(paramInt)
            this.memory.fireTableRowsUpdated(i, i)
            this.memory.fireTableRowsUpdated(paramInt, paramInt)
        }
    var psr: Int
        get() = this.registerFile.psr
        set(paramInt) {
            this.registerFile.psr = paramInt
        }

    var mcr: Int
        get() = this.registerFile.mcr
        set(paramInt) {
            this.registerFile.mcr = paramInt
        }

    var clockMCR: Boolean
        get() = this.registerFile.clockMCR
        set(paramBoolean) {
            this.registerFile.clockMCR = paramBoolean
        }

    var mpr: Int
        get() = this.registerFile.mpr
        set(paramInt) {
            this.registerFile.mpr = paramInt
        }

    init {
        this.memory = Memory(this)
        this.registerFile = RegisterFile(this)
        this.NotifyOnStop = LinkedList()
        this.pc = 512
    }

    fun setGUI(paramTC3GUI: TC3GUI) {
        this.gui = paramTC3GUI
    }

    fun setStoppedListener(paramActionListener: ActionListener) {
        this.NotifyOnStop.add(paramActionListener)
    }

    @JvmOverloads
    fun scrollToPC(paramInt: Int = 0) {
        if (this.gui != null)
            this.gui!!.scrollToPC(paramInt)
    }

    fun reset() {
        this.symbolTable.clear()
        this.inverseTable.clear()
        this.memory.reset()

        if (this.gui != null)
            this.gui!!.setTextConsoleEnabled(true)

        this.registerFile.reset()
        this.CHECKS_PASSED = 0
        this.CHECKS_FAILED = 0
        pc = 512
        scrollToPC()
    }

    val keyBoardDevice: KeyboardDevice
        get() = this.memory.kbDevice

    fun loadSymbolTable(paramFile: File): String {

        try {
            val localBufferedReader = BufferedReader(FileReader(paramFile))
            var i = 0

            while (localBufferedReader.ready()) {
                val str2 = localBufferedReader.readLine()
                i++
                if (i >= 5) {
                    val arrayOfString = str2.split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    if (arrayOfString.size >= 3) {
                        val localInteger = Word.parseNum("x" + arrayOfString[2])
                        this.symbolTable.put(arrayOfString[1].toLowerCase(), localInteger)
                        this.inverseTable.put(localInteger, arrayOfString[1])
                    }
                }
            }
            str1 = "Loaded symbol file '" + paramFile.path + "'"
        } catch (localIOException: IOException) {
            return "Could not load symbol file '" + paramFile.path + "'"
        }

        return str1
    }

    fun setContinueMode() {
        this.isContinueMode = true
    }

    fun clearContinueMode() {
        this.isContinueMode = false
    }

    fun loadObjectFile(paramFile: File): String {
        val arrayOfByte = ByteArray(2)
        var str2 = paramFile.path

        try {
            val localFileInputStream = FileInputStream(paramFile)
            localFileInputStream.read(arrayOfByte)
            var i = Word.convertByteArray(arrayOfByte[0], arrayOfByte[1])

            while (localFileInputStream.read(arrayOfByte) == 2) {
                val localInteger = i
                if (this.symbolTable.contains(localInteger)) {
                    val str4 = this.inverseTable.get(localInteger) as String
                    this.symbolTable.remove(str4.toLowerCase())
                    this.inverseTable.remove(localInteger)
                }
                this.memory.write(i, Word.convertByteArray(arrayOfByte[0], arrayOfByte[1]))
                i++
            }
            localFileInputStream.close()
            str2 = "Loaded object file '$str2'"
        } catch (localIOException: IOException) {
            return "Could not load object file '$str2'"
        }

        var str3 = str2
        if (str2.endsWith(".obj")) {
            str3 = str2.substring(0, str2.length - 4)
        }
        str3 = str3 + ".sym"
        str1 = String.format("%s\n%s", str1, loadSymbolTable(File(str3)))

        return str1
    }

    fun readMemory(paramInt: Int): Word {
        return this.memory.read(paramInt)!!
    }

    fun writeMemory(paramInt1: Int, paramInt2: Int) {
        this.memory.write(paramInt1, paramInt2)
    }

    fun setBreakPoint(paramString: String): String {
        return this.memory.setBreakPoint(paramString)
    }

    fun isBreakPoint(paramInt: Int): Boolean {
        return this.memory.breakPointSet(paramInt)
    }

    fun clearBreakPoint(paramString: String): String {
        return this.memory.clearBreakPoint(paramString)
    }

    fun setKeyboardInputStream(paramFile: File): String {
        var str: String
        try {
            this.memory.kbDevice.setInputStream(FileInputStream(paramFile))
            this.memory.kbDevice.setInputMode(KeyboardDevice.SCRIPT_MODE)
            str = "Keyboard input file '" + paramFile.path + "' enabled"
            if (this.gui != null)
                this.gui!!.setTextConsoleEnabled(false)
        } catch (localFileNotFoundException: FileNotFoundException) {
            str = "Could not open keyboard input file '" + paramFile.path + "'"
            if (this.gui != null)
                this.gui!!.setTextConsoleEnabled(true)
        }

        return str
    }

    fun printRegisters(): String {
        return this.registerFile.toString()
    }

    fun setZ() {
        this.registerFile.setZ()
    }

    fun setN() {
        this.registerFile.setN()
    }


    fun setP() {
        this.registerFile.setP()
    }


    fun getRegister(paramInt: Int): Int {
        return this.registerFile.getRegister(paramInt)
    }

    fun setRegister(paramInt1: Int, paramInt2: Int) {
        this.registerFile.write(paramInt1, paramInt2)
    }

    @Throws(IllegalMemAccessException::class)
    fun checkAddr(paramInt: Int) {
        val bool = this.registerFile.privMode

        if (paramInt < 0 || paramInt >= 65535)
            throw IllegalMemAccessException(paramInt)

        if (bool) return
        val i = paramInt shr 12
        val j = 1 shl i
        val k = mpr

        if (j and k == 0)
            throw IllegalMemAccessException(paramInt)
    }

    val currentInst: Word
        get() = this.memory.getInst(pc)

    fun incPC(paramInt: Int) {
        val i = this.registerFile.pc.getValue()
        pc = i + paramInt
    }

    fun addNextBreakPoint(paramInt: Int) {
        this.nextBreakPoints.add(paramInt)
    }

    fun nextBreakPointSet(paramInt: Int): Boolean {
        return this.nextBreakPoints.contains(paramInt)
    }

    fun removeNextBreakPoint(paramInt: Int) {
        this.nextBreakPoints.remove(paramInt)
    }

    @Throws(CustomException::class)
    fun executeStep() {
        checkAddr(pc)
        clockMCR = true
        Instruction.execute(currentInst, this)
        updateStatusLabel()
        scrollToPC()
    }

    @Throws(CustomException::class)
    fun executeNext() {
        if (Instruction.nextOver(readMemory(pc))) {
            addNextBreakPoint((pc + 1) % 65535)
            executeMany()
        } else
            executeStep()
    }

    @Synchronized
    fun stopExecution(paramBoolean: Boolean): String {
        return stopExecution(0, paramBoolean)
    }

    @Synchronized
    fun stopExecution(paramInt: Int, paramBoolean: Boolean): String {
        this.stopImmediately = true
        clearContinueMode()
        updateStatusLabel()
        scrollToPC(paramInt)
        this.memory.fireTableDataChanged()

        if (paramBoolean) {
            val localListIterator = this.NotifyOnStop.listIterator(0)
            while (localListIterator.hasNext()) {
                val localActionListener = localListIterator.next() //as ActionListener
                localActionListener.actionPerformed(null)
            }
        }
        return "Stopped at ${Word.toHex(pc)}"
    }

    @Throws(CustomException::class)
    fun executePumpedContinues() {
        var i = 400
        clockMCR = true
        if (this.gui != null)
            this.gui!!.setStatusLabelRunning()
        while (!this.stopImmediately && i > 0) {
            try {
                checkAddr(pc)
                Instruction.execute(currentInst, this)
            } catch (localCustomException: CustomException) {
                stopExecution(true)
                throw localCustomException
            }

            if (this.memory.breakPointSet(pc)) {
                val str = "Hit breakpoint at ${Word.toHex(pc)}"
                if (this.gui == null)
                    println(str)
                else
                    CommandLinePanel.writeToConsole(str)
                stopExecution(true)
            }
            if (nextBreakPointSet(pc)) {
                stopExecution(true)
                removeNextBreakPoint(pc)
            }
            i--
        }
        if (isContinueMode)
            SwingUtilities.invokeLater(this)
    }

    @Synchronized
    @Throws(CustomException::class)
    fun executeMany() {
        setContinueMode()
        this.stopImmediately = false
        try {
            executePumpedContinues()
        } catch (localCustomException: CustomException) {
            stopExecution(true)
            throw localCustomException
        }

    }

    fun lookupSym(paramInt: Int): String {
        return this.inverseTable[paramInt].toString()
    }

    fun lookupSym(paramString: String): Int {
        val localObject = this.symbolTable[paramString.toLowerCase()]
        return if (localObject != null) (localObject.toInt()) else Integer.MAX_VALUE
    }

    fun getAddress(paramString: String): Int {
        var i = Word.parseNum(paramString)
        if (i == Integer.MAX_VALUE)
            i = lookupSym(paramString)
        return i
    }

    override fun run() {
        try {
            executePumpedContinues()
        } catch (localCustomException: CustomException) {
            if (this.gui != null)
                localCustomException.showMessageDialog(this.gui!!.frame)
            else
                println(localCustomException.message)
        }

    }

    fun updateStatusLabel() {
        if (this.gui != null) {
            if (!clockMCR)
                this.gui!!.setStatusLabelHalted()
            else if (isContinueMode)
                this.gui!!.setStatusLabelRunning()
            else
                this.gui!!.setStatusLabelSuspended()
        }
    }

    companion object {
        val MEM_SIZE = 65535
        val MONITOR_READY = Word(32768)
        val MONITOR_NOTREADY = Word(0)
        val KB_AVAILABLE = Word(32768)
        val KB_UNAVAILABLE = Word(0)
        val TIMER_SET = Word(32768)
        val TIMER_UNSET = Word(0)
        val NUM_CONTINUES = 400
    }
}