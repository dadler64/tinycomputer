package com.tc

class IllegalInstructionException(paramString: String) : CustomException(paramString) {

    override val exceptionDescription: String
        get() = "tc.IllegalInstructionException: $message"

    companion object {
        private val serialVersionUID = 101L
    }
}