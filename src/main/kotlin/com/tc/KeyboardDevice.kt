package com.tc

import java.io.BufferedReader
import java.io.IOException

class KeyboardDevice {
    private var kbin: BufferedReader? = null
    private var defkbin: BufferedReader? = null
    private var current = 0
    private var mode: Int = 0
    private var defmode: Int = 0

    init {
        this.kbin = BufferedReader(java.io.InputStreamReader(System.`in`))
        this.mode = INTERACTIVE_MODE
    }

    fun setDefaultInputStream() {
        this.defkbin = this.kbin
    }

    fun setDefaultInputMode() {
        this.defmode = this.mode
    }

    fun setInputStream(paramInputStream: java.io.InputStream) {
        this.kbin = BufferedReader(java.io.InputStreamReader(paramInputStream))
    }

    fun setInputMode(paramInt: Int) {
        this.mode = paramInt
    }

    fun reset() {
        this.kbin = this.defkbin
        this.mode = this.defmode
        this.current = 0
    }

    fun available(): Boolean {
        try {
            if (this.kbin!!.ready()) {
                this.kbin!!.mark(1)
                if (this.kbin!!.read() == TIMER_TICK.toInt()) {
                    this.kbin!!.reset()
                    return false
                }
                this.kbin!!.reset()
                return true
            }
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

        return false
    }

    fun read(): Word {
        val arrayOfChar = CharArray(CBUFSIZE)
        try {
            if (available()) {
                if (this.mode == INTERACTIVE_MODE) {
                    val i = this.kbin!!.read(arrayOfChar, 0, CBUFSIZE)
                    this.current = arrayOfChar[i - 1].toInt()
                } else {
                    this.current = this.kbin!!.read()
                }
            }
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

        return Word(this.current)
    }

    fun hasTimerTick(): Boolean {
        try {
            this.kbin!!.mark(1)
            if (this.kbin!!.ready()) {
                if (this.kbin!!.read() == TIMER_TICK.toInt())
                    return true
                this.kbin!!.reset()
                return false
            }
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

        return false
    }

    companion object {

        var SCRIPT_MODE = 0
        var INTERACTIVE_MODE = 1
        private val CBUFSIZE = 128
        private val TIMER_TICK = '.'
    }
}