package com.tc

class TimerDevice {
    private var mode: Int = 0
    /**
     * Get if the timer is active
     */
    /**
     * Set whether or not the timer is active
     */
    var isEnabled = false
    private var lastTime: Long = 0
    /**
     * Get the timer interval
     */
    var interval: Long = 0
        private set
    private var kb: KeyboardDevice? = null

    init {
        this.mode = AUTOMATIC_TIMER
        this.isEnabled = true
    }

    /**
     * Set the timer
     */
    fun setTimer() {
        this.mode = AUTOMATIC_TIMER
        this.interval = TIMER_INTERVAL
        this.lastTime = System.currentTimeMillis()
    }

    fun setTimer(paramLong: Long) {
        this.mode = AUTOMATIC_TIMER
        this.interval = paramLong
        this.lastTime = System.currentTimeMillis()
    }

    fun setTimer(paramKeyboardDevice: KeyboardDevice) {
        this.mode = MANUAL_TIMER
        this.interval = 1L
        this.kb = paramKeyboardDevice
    }

    fun reset() {
        this.mode = AUTOMATIC_TIMER
        setTimer(TIMER_INTERVAL)
    }

    fun hasGoneOff(): Boolean {
        if (!this.isEnabled) {
            return false
        }

        if (this.mode == AUTOMATIC_TIMER) {
            val l = System.currentTimeMillis()
            if (l - this.lastTime > this.interval) {
                this.lastTime = l
                return true
            }
            return false
        }

        return this.kb!!.hasTimerTick()
    }

    companion object {
        private val MANUAL_TIMER = 0
        private val AUTOMATIC_TIMER = 1
        private val TIMER_INTERVAL = 500L
    }
}
