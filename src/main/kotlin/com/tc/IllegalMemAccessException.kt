package com.tc

class IllegalMemAccessException(private val addr: Int) : CustomException() {

    override val exceptionDescription: String
        get() =
            "tc.IllegalMemAccessException accessing address " + Word.toHex(this.addr) + "\n" + "(The MPR and PSR do not permit access to this address)"

    companion object {
        private val serialVersionUID = 108L
    }
}