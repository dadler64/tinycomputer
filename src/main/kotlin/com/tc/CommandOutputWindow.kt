package com.tc

import javax.swing.JFrame
import javax.swing.JScrollPane
import javax.swing.JTextArea
import javax.swing.text.BadLocationException

class CommandOutputWindow(paramString: String) : JFrame(paramString) {

    init {
        textArea = JTextArea()
        textArea.isEditable = false
        textArea.lineWrap = true
        textArea.wrapStyleWord = true
        val localJScrollPane = JScrollPane(textArea, 22, 30)

        contentPane.add(localJScrollPane)
    }

    companion object {
        private val serialVersionUID = 1L
        private lateinit var textArea: JTextArea

        fun writeToConsole(paramString: String) {
            // [DEBUG] Print what is going to the console to terminal
            println("[Console]: $paramString")
            textArea.append(paramString + "\n")
        }

        fun clearConsole() {
            val localDocument = textArea.document
            try {
                localDocument.remove(0, localDocument.length)
            } catch (localBadLocationException: BadLocationException) {
                ErrorLog.logError(localBadLocationException)
            }

        }
    }
}
