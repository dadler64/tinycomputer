package com.tc

import java.awt.Dimension
import java.awt.GridBagConstraints
import java.awt.GridBagLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import javax.swing.*
import javax.swing.text.BadLocationException

class CommandLinePanel(private val mac: Machine) : JPanel(GridBagLayout()), ActionListener {
    lateinit var cmd: CommandLine
    private var textField: JTextField = JTextField(20)
    private lateinit var gui: TC3GUI

    init {
        this.textField.addActionListener(this)

        this.textField.inputMap.put(KeyStroke.getKeyStroke("UP"), "prevHistory")
        this.textField.inputMap.put(KeyStroke.getKeyStroke("DOWN"), "nextHistory")
        this.textField.actionMap.put("prevHistory", object : AbstractAction() {
            private val serialVersionUID = 1L

            override fun actionPerformed(paramAnonymousActionEvent: ActionEvent) {
                this@CommandLinePanel.textField.text = this@CommandLinePanel.cmd.prevHistory
            }
        })

        this.textField.actionMap.put("nextHistory", object : AbstractAction() {
            private val serialVersionUID = 1L

            override fun actionPerformed(paramAnonymousActionEvent: ActionEvent) {
                this@CommandLinePanel.textField.text = this@CommandLinePanel.cmd.nextHistory
            }
        })
        this.cmd = CommandLine(mac)
        textArea = JTextArea(5, 70)
        textArea.isEditable = false
        textArea.lineWrap = true
        textArea.wrapStyleWord = true
        val localJScrollPane = JScrollPane(textArea, 22, 30)

        var localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridwidth = 0
        localGridBagConstraints.fill = 2
        add(this.textField, localGridBagConstraints)

        localGridBagConstraints = GridBagConstraints()
        localGridBagConstraints.gridwidth = 0
        localGridBagConstraints.fill = 1
        localGridBagConstraints.weightx = 1.0
        localGridBagConstraints.weighty = 1.0
        add(localJScrollPane, localGridBagConstraints)

        minimumSize = Dimension(20, 1)
    }

    fun setGUI(paramTC3GUI: TC3GUI) {
        this.cmd.setGUI(paramTC3GUI)
        this.gui = paramTC3GUI
    }

    fun clear() {
        val localDocument = textArea.document
        try {
            localDocument.remove(0, localDocument.length)
        } catch (localBadLocationException: BadLocationException) {
            ErrorLog.logError(localBadLocationException)
        }
        CommandOutputWindow.clearConsole()
    }

    override fun actionPerformed(paramActionEvent: ActionEvent?) {
        var str: String?

        if (paramActionEvent != null) {
            str = this.textField.text
            this.cmd.scheduleCommand(str)

            while (this.cmd.hasMoreCommands() && (!this.mac.isContinueMode || this.cmd.hasQueuedStop())) {
                try {
                    str = this.cmd.runCommand(this.cmd.nextCommand)

                    if (str.isNotBlank() || str.isNotEmpty())
                        writeToConsole(str)
                    else
                        this.gui.confirmExit()
                } catch (localCustomException: CustomException) {
                    localCustomException.showMessageDialog(parent)
                }

            }
            this.textField.selectAll()
            textArea.caretPosition = textArea.document.length
        }
    }

    companion object {
        private val serialVersionUID = 104L
        private val newline = "\n"
        protected lateinit var textArea: JTextArea

        fun writeToConsole(paramString: String) {
            textArea.append("$paramString$newline")
//            writeToConsole(paramString)
            CommandOutputWindow.writeToConsole(paramString)
        }
    }
}
