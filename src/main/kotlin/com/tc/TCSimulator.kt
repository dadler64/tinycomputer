package com.tc

import java.io.BufferedReader

object TCSimulator {
    var GRAPHICAL_MODE = true
    //    private val version = "1.0.5"
    private val version = "1.1.0"

    fun getVersion(): String {
        return "TC-3 Simulator Version $version"
    }

    @JvmStatic
    fun main(paramArrayOfString: Array<String>) {
        ErrorLog.logInit()
        var str1: String = ""
        if (paramArrayOfString.isNotEmpty()) {
            if ("-t" == paramArrayOfString[0]) {
                GRAPHICAL_MODE = false
                if (paramArrayOfString.size > 2 && "-s" == paramArrayOfString[1]) {
                    str1 = paramArrayOfString[2]
                }
            }
            if ("-h" == paramArrayOfString[0]) {
                println("Usage: tc.TCSimulator [-t [-s script]]")
                println("-t : start in command-line mode")
                println("-s script : run 'script' as a script file (only available in command-line mode)")
            }
        }
        val localMachine = Machine()
        val localObject: Any
        if (GRAPHICAL_MODE) {
            TC3GUI.initLookAndFeel()
            localObject = TC3GUI(localMachine)

            localMachine.setGUI(localObject as TC3GUI)
            javax.swing.SwingUtilities.invokeLater(TempRun(localObject as TC3GUI))
        } else {
            localObject = CommandLine(localMachine)
            try {
                val localBufferedReader = BufferedReader(java.io.InputStreamReader(System.`in`))
                var str3: String? = null
                if (str1 != null)
                    localObject.scheduleCommand("@script $str1")
                while (true) {
                    if (!localMachine.isContinueMode) {
                        print("\n==>")
                    }

                    if (str1 == null) {
                        val str2 = localBufferedReader.readLine()
                        if (str2 != null)
                            localObject.scheduleCommand(str2)
                    }
                    while (localObject.hasMoreCommands() && (!localMachine.isContinueMode || localObject.hasQueuedStop())) {
                        val str4 = localObject.nextCommand
                        if (str1 != null && !str4.startsWith("@")) {
//                            str1 = null
                            str1 = "nil"
                        }
                        str3 = try {
                            localObject.runCommand(str4)
                        } catch (localCustomException: CustomException) {
                            localCustomException.exceptionDescription
                        } catch (localNumberFormatException: NumberFormatException) {
                            "NumberFormatException: ${localNumberFormatException.message}"
                        }

                        if (str3 == null) {
                            println("Bye!")
                            ErrorLog.logClose()
                            return
                        }
                        println(str3)
                    }

                    if (str1 != null && !localObject.hasMoreCommands())
//                        str1 = null
                        str1 = "nil"
                    return
                }
            } catch (localIOException: java.io.IOException) {
                ErrorLog.logError(localIOException)
            }

        }
    }
}





