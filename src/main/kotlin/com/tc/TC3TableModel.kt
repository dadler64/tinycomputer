package com.tc

import javax.swing.event.TableModelEvent
import javax.swing.table.AbstractTableModel

abstract class TC3TableModel : AbstractTableModel() {

    override fun fireTableCellUpdated(paramInt1: Int, paramInt2: Int) {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableCellUpdated(paramInt1, paramInt2)
    }

    override fun fireTableChanged(paramTableModelEvent: TableModelEvent) {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableChanged(paramTableModelEvent)
    }

    override fun fireTableDataChanged() {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableDataChanged()

    }

    override fun fireTableRowsUpdated(paramInt1: Int, paramInt2: Int) {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableRowsUpdated(paramInt1, paramInt2)
    }

    override fun fireTableRowsInserted(paramInt1: Int, paramInt2: Int) {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableRowsInserted(paramInt1, paramInt2)
    }

    override fun fireTableRowsDeleted(paramInt1: Int, paramInt2: Int) {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableRowsDeleted(paramInt1, paramInt2)
    }

    override fun fireTableStructureChanged() {
        if (TCSimulator.GRAPHICAL_MODE)
            super.fireTableStructureChanged()
    }
}
