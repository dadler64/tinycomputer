package com.tc

import java.io.*
import java.util.*
import kotlin.collections.ArrayList

class CommandLine(private val mac: Machine) {
    val PROMPT = "\n==>"
    private val comp = object : ThreadLocal<Comparator<Any>>() {
        override fun initialValue(): Comparator<Any> {
            return object : Comparator<Any> {
                override fun compare(paramAnonymousObject1: Any, paramAnonymousObject2: Any): Int {
                    val localCommand1 = paramAnonymousObject1 as Command
                    val localCommand2 = paramAnonymousObject2 as Command
                    val str1 = localCommand1.usage.split("\\s+")[0]
                    val str2 = localCommand2.usage.split("\\s+")[0]
                    return str1.compareTo(str2)
                }

                override fun equals(other: Any?): Boolean {
                    val localCommand = other as Command?

                    val str1 = localCommand!!.usage.split("\\s+")[0]
                    val str2 = this.toString().split("\\s+".toRegex()).dropLastWhile {
                        it.isEmpty()
                    }.toTypedArray()[0]
                    return str1 == str2
                }
            }
        }
    }
    private lateinit var GUI: TC3GUI
    private var commandQueue: LinkedList<String>
    private var prevHistoryStack: Stack<String>
    private var nextHistoryStack: Stack<String>
    private var commands: Hashtable<String, Command> = Hashtable()
    private var commandsSet: TreeSet<Command>

    init {
        setupCommands()
        this.commandsSet = TreeSet(object : Comparator<Any> {
            override fun compare(paramAnonymousObject1: Any, paramAnonymousObject2: Any): Int {
                val localCommand1 = paramAnonymousObject1 as Command
                val localCommand2 = paramAnonymousObject2 as Command
                val str1 = localCommand1.usage.split("\\s+")[0]
                val str2 = localCommand2.usage.split("\\s+")[0]
                return str1.compareTo(str2)
            }

            override fun equals(other: Any?): Boolean {
                val localCommand = other as Command?

                val str1 = localCommand!!.usage.split("\\s+")[0]
                val str2 = this.toString().split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()[0]
                // TODO: Figure out if this is an error and if not if it should be labeled as 'str3'
//                val str2 = (this as tc.Command).usage.split("\\s+")[0]
                return str1 == str2
            }
        })
        commandsSet = TreeSet(comp.get())
        this.commandsSet.addAll(this.commands.values)

        this.commandQueue = LinkedList()
        this.prevHistoryStack = Stack()
        this.nextHistoryStack = Stack()
    }

    fun scheduleCommand(paramString: String) {
        if (paramString.equals("stop", ignoreCase = true)) {
            this.commandQueue.addFirst(paramString)
        } else {
            this.commandQueue.add(paramString)
        }
    }

    fun scheduleScriptCommands(paramArrayList: ArrayList<String>) {
        val localListIterator = paramArrayList.listIterator(paramArrayList.size)
        while (localListIterator.hasPrevious()) {
            val str = localListIterator.previous()
            this.commandQueue.addFirst(str)
        }
    }

    fun hasMoreCommands(): Boolean {
        return this.commandQueue.size != 0
    }

    val nextCommand: String
        get() = this.commandQueue.removeFirst()

    fun hasQueuedStop(): Boolean {
        return this.commandQueue.first.equals("stop", ignoreCase = true)
    }

    private fun addToHistory(paramString: String) {
        if (this.prevHistoryStack.empty())
            this.prevHistoryStack.push(paramString)
        else if (this.prevHistoryStack.peek() != paramString)
            this.prevHistoryStack.push(paramString)

    }

    val prevHistory: String
        get() {
            if (this.prevHistoryStack.empty()) {
                return "nil"
            }
            val str = this.prevHistoryStack.pop()
            this.nextHistoryStack.push(str)
            return str
        }

    val nextHistory: String
        get() {
            if (this.nextHistoryStack.empty()) {
                return "nil"
            }
            val str = this.nextHistoryStack.pop()
            this.prevHistoryStack.push(str)
            return str
        }

    private fun resetHistoryStack() {
        while (!this.nextHistoryStack.empty()) {
            this.prevHistoryStack.push(this.nextHistoryStack.pop())
        }
    }

    // TODO Review, Document and Add more commands.
    private fun setupCommands() {
        this.commands.put("help", object : Command {
            override val usage: String
                get() = "h[elp] [command]"

            override val help: String
                get() = "Print out help for all available commands, or for just a specified command."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt > 2)
                    return usage
                if (paramInt == 1) {
                    var localObject = ""
                    val localIterator = this@CommandLine.commandsSet.iterator()

                    while (localIterator.hasNext()) {
                        val localCommand = localIterator.next()
                        val str1 = localCommand.usage
                        val str2 = str1.split("\\s+".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()[0]
                        localObject = localObject + str2 + " usage: " + str1 + "\n"
                    }
                    return localObject
                }
                val localObject = this@CommandLine.commands[paramArrayOfString[1].toLowerCase()] ?: return paramArrayOfString[1] + ": command not found"

                return "usage: " + localObject.usage + "\n   " + localObject.help
            }
        })
        this.commands.put("h", this.commands["help"])

        this.commands.put("quit", object : Command {
            override val usage: String
                get() = "quit"

            override val help: String
                get() = "Quit the simulator."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                return if (paramInt != 1)
                    usage
                else "Error: nil"
            }
        })

        this.commands.put("next", object : Command {

            override val usage: String
                get() = "n[ext]"

            override val help: String
                get() = "Executes the next instruction."

            @Throws(CustomException::class)
            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt != 1)
                    return usage
                this@CommandLine.mac.executeNext()
                return ""
            }
        })
        this.commands.put("n", this.commands["next"])

        this.commands.put("step", object : Command {
            override val usage: String
                get() = "s[tep]"

            override val help: String
                get() = "Steps into the next instruction."

            @Throws(CustomException::class)
            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                this@CommandLine.mac.executeStep()
                return ""
            }
        })
        this.commands.put("s", this.commands["step"])

        this.commands.put("continue", object : Command {
            override val usage: String
                get() = "c[ontinue]"

            override val help: String
                get() = "Continues running instructions until next breakpoint is hit."

            @Throws(CustomException::class)
            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                println("use the 'stop' command to interrupt execution")
                CommandLinePanel.writeToConsole("use the 'stop' command to interrupt execution")

                this@CommandLine.mac.executeMany()
                return ""
            }
        })
        this.commands.put("c", this.commands["continue"])

        this.commands.put("stop", object : Command {
            override val usage: String
                get() = "stop"

            override val help: String
                get() = "Stops execution temporarily."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                return this@CommandLine.mac.stopExecution(true)
            }
        })

        this.commands.put("reset", object : Command {
            override val usage: String
                get() = "reset"

            override val help: String
                get() = "Resets the machine and simulator."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt != 1)
                    return usage

                this@CommandLine.mac.stopExecution(false)
                this@CommandLine.mac.reset()
                return "System reset"
            }
        })

        this.commands.put("print", object : Command {
            override val usage: String
                get() = "p[rint]"

            override val help: String
                get() = "Prints out all registers, PC, MPR and PSR."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                return if (paramInt != 1) {
                    usage
                } else this@CommandLine.mac.printRegisters()
            }
        })
        this.commands.put("p", this.commands["print"])

        this.commands.put("input", object : Command {
            override val usage: String
                get() = "input <filename>"

            override val help: String
                get() =
                    "Specifies a file to read the input from instead of keyboard device (simulator must be restarted to restore normal keyboard input)."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                return if (paramInt != 2)
                    usage
                else this@CommandLine.mac.setKeyboardInputStream(File(paramArrayOfString[1]))
            }
        })

        this.commands.put("break", object : Command {
            override val usage: String
                get() = "b[reak] [ set | clear ] [ mem_addr | label ]"

            override val help: String
                get() = "Sets or clears break point at specified memory address or label."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt != 3)
                    return usage
                if (paramArrayOfString[1].toLowerCase() == "set")
                    return this@CommandLine.mac.setBreakPoint(paramArrayOfString[2])
                return if (paramArrayOfString[1].toLowerCase().equals("clear", ignoreCase = true))
                    this@CommandLine.mac.clearBreakPoint(paramArrayOfString[2])
                else usage
            }
        })
        this.commands.put("b", this.commands["break"])

        this.commands.put("script", object : Command {
            override val usage: String
                get() = "script <filename>"

            override val help: String
                get() = "Specifies a file from which to read commands."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt != 2)
                    return usage
                val localFile = File(paramArrayOfString[1])

                try {
                    val localBufferedReader = BufferedReader(FileReader(localFile))
                    val localArrayList = ArrayList<String>()

                    while (true) {
                        val str = localBufferedReader.readLine() ?: break
                        localArrayList.add("@$str")
                    }

                    this@CommandLine.scheduleScriptCommands(localArrayList)
                } catch (localIOException: IOException) {
                    return localIOException.message.toString()
                }
                return ""
            }
        })

        this.commands.put("load", object : Command {
            override val usage: String
                get() = "l[oa]d <filename>"

            override val help: String
                get() = "Loads an object file into the memory."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                return if (paramInt != 2) {
                    usage
                } else this@CommandLine.mac.loadObjectFile(File(paramArrayOfString[1]))
            }
        })
        this.commands.put("ld", this.commands["load"])

        this.commands.put("check", object : Command {
            override val usage: String
                get() =
                    "check [ count | reset | PC | reg | PSR | MPR | mem_addr | label | N | Z | P ] [ mem_addr | label ] [ value | label ]"

            override val help: String
                get() {
                    var str = "Verifies that a particular value resides in a register or in a memory location, or that a condition code is set.\n"
                    str += "Samples:\n"
                    str += "'check PC LABEL' checks if the PC points to wherever LABEL points.\n"
                    str += "'check LABEL VALUE' checks if the value stored in memory at the location pointed to by LABEL is equal to VALUE.\n"
                    str += "'check VALUE LABEL' checks if the value stored in memory at VALUE is equal to the location pointed to by LABEL (probably not very useful). To find out where a label points, use 'list' instead.\n"
                    return str
                }

            private fun check(paramAnonymousBoolean: Boolean, paramAnonymousArrayOfString: Array<String>, paramAnonymousString: String?): String {
                val str1 = "TRUE"
                val str2 = "FALSE"
                var str3 = "("
                for (i in paramAnonymousArrayOfString.indices) {
                    str3 += paramAnonymousArrayOfString[i]
                    str3 += if (i == paramAnonymousArrayOfString.size - 1) ")" else " "
                }

                if (paramAnonymousBoolean) {
                    this@CommandLine.mac.CHECKS_PASSED += 1
                    return "$str1 $str3"
                }
                this@CommandLine.mac.CHECKS_FAILED += 1
                return "$str2 $str3 (actual value: $paramAnonymousString)"
            }

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt < 2 || paramInt > 4) {
                    return usage
                }
                if (paramInt == 2) {
                    if (paramArrayOfString[1] == "count") {
                        // TODO: Check if this need to be global instead of local
                        val localObject = if (this@CommandLine.mac.CHECKS_PASSED ==  1) "check" else "checks"
                        //return main.java.com.rainwire.tc3.tc.CommandLine.this.mac.CHECKS_PASSED + " " + (String)localObject + " passed, " + main.java.com.rainwire.tc3.tc.CommandLine.this.mac.CHECKS_FAILED + " failed";
                        return "${this@CommandLine.mac.CHECKS_PASSED} $localObject passed, ${this@CommandLine.mac.CHECKS_FAILED} failed"
                    }
                    if (paramArrayOfString[1] == "reset") {
                        this@CommandLine.mac.CHECKS_PASSED = 0
                        this@CommandLine.mac.CHECKS_FAILED = 0
                        return "check counts reset"
                    }
                    val localObject = this@CommandLine.mac.registerFile
                    return if (paramArrayOfString[1].toLowerCase() == "n" && localObject.n || paramArrayOfString[1].toLowerCase() == "z" && localObject.z || paramArrayOfString[1].toLowerCase() == "p" && localObject.p) {
                        check(true, paramArrayOfString, "")
                    } else check(false, paramArrayOfString, localObject.printCC())
                }

                var i = Word.parseNum(paramArrayOfString[paramInt - 1])
                if (i == Int.MAX_VALUE) {
                    i = this@CommandLine.mac.lookupSym(paramArrayOfString[paramInt - 1])
                    if (i == Int.MAX_VALUE) {
                        return "Bad value or label: ${paramArrayOfString[paramInt - 1]}"
                    }
                }
                val localBoolean = this@CommandLine.checkRegister(paramArrayOfString[1], i)!!
                if (localBoolean != null) {
                    return check(localBoolean, paramArrayOfString, this@CommandLine.getRegister(paramArrayOfString[1]))
                }

                val j = this@CommandLine.mac.getAddress(paramArrayOfString[1])

                if (j == Int.MAX_VALUE) {
                    return "Bad register, value or label: ${paramArrayOfString[1]}"
                }

                if (j < 0 || j >= 0xFFFF) {
                    return "Address ${paramArrayOfString[1]} out of bounds"
                }
                val k: Int
                if (paramInt == 3) {
                    k = j
                } else {
                    k = this@CommandLine.mac.getAddress(paramArrayOfString[2])
                    if (k == Int.MAX_VALUE) {
                        return "Bad register, value or label: ${paramArrayOfString[2]}"
                    }
                    if (k < 0 || k >= 0xFFFF) {
                        return "Address ${paramArrayOfString[2]} out of bounds"
                    }
                    if (k < j) {
                        return "Second address in range (${paramArrayOfString[2]}) must be >= first (${paramArrayOfString[1]})"
                    }
                }

                var localWord: Word?
                var bool = true
                var str = ""
                for (m in j..k) {
                    localWord = this@CommandLine.mac.readMemory(m)
                    if (localWord != null) {
                        if (localWord.getValue() != (i and 0xFFFF)) {
                            bool = false
                            str += if (str.isEmpty()) "" else ", "
                            str = str + Word.toHex(m) + ":" + localWord.toHex()
                        }
                    } else {
                        return "Bad register, value or label: " + paramArrayOfString[1]
                    }
                }
                return check(bool, paramArrayOfString, str)
            }
        })

        //TODO: Fix 'dump' command
        this.commands.put("dump", object : Command {
            override val usage: String
                get() = "d[ump] [-c] mem_addr mem_addr dumpfile"

            override val help: String
                get() = "dumps a range of memory values to a specified file as raw values or (with the -c flag) as 'check' commands"

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt < 4 || paramInt > 5) {
                    return usage
                }
                if (paramInt == 5 && paramArrayOfString[1] != "-c") {
                    println(paramArrayOfString[1])
                    return usage
                }
//                val localIOException1 = this@main.java.com.rainwire.tc3.tc.CommandLine.mac.getAddress(paramAnonymousArrayOfString[paramAnonymousInt - 3])
//                val localIOException2 = this@main.java.com.rainwire.tc3.tc.CommandLine.mac.getAddress(paramAnonymousArrayOfString[paramAnonymousInt - 2])

                val localIOException1 = this@CommandLine.mac.getAddress(paramArrayOfString[paramInt - 3])
                val localIOException2 = this@CommandLine.mac.getAddress(paramArrayOfString[paramInt - 2])
                var localIOException3 = this@CommandLine.mac.getAddress(paramArrayOfString[paramInt - 1])

                if (localIOException1 == Int.MAX_VALUE) {
                    return "Error: Invalid register, address, or label  ('${paramArrayOfString[paramInt - 3]}')"
                }
                if (localIOException1 < 0 || localIOException1 >= 65535) {
                    return "Address ${paramArrayOfString[paramInt - 3]} out of bounds"
                }
                if (localIOException2 == Int.MAX_VALUE) {
                    return "Error: Invalid register, address, or label  ('${paramArrayOfString[paramInt - 3]}')"
                }
                if (localIOException2 < 0 || localIOException2 >= 65535) {
                    return "Address ${paramArrayOfString[paramInt - 2]} out of bounds"
                }
                if (localIOException2 < localIOException1) {
                    return "Second address in range (${paramArrayOfString[paramInt - 2]}) must be >= first (${paramArrayOfString[paramInt - 3]})"
                }

                var localWord: Word? = null
                val localFile = File(paramArrayOfString[paramInt - 1])
                val localPrintWriter: PrintWriter

                try {
                    if (!localFile.createNewFile()) {
                        return "File ${paramArrayOfString[paramInt - 1]} already exists. Choose a different filename."
                    }
                    localPrintWriter = PrintWriter(BufferedWriter(FileWriter(localFile)))
                } catch (localIOException3: IOException) {
                    ErrorLog.logError(localIOException3)
                    return "Error opening file: ${localFile.name}"
                }

                localIOException3 = localIOException1
                while (localIOException3 <= localIOException2) {
                    localWord = this@CommandLine.mac.readMemory(localIOException3)
                    if (localWord != null) {
                        if (paramInt == 5) {
                            localPrintWriter.println("check ${Word.toHex(localIOException3)} ${localWord!!.toHex()}")
                        } else {
                            localPrintWriter.println(localWord!!.toHex())
                        }
                    } else {
                        return "Bad register, value or label: ${paramArrayOfString[paramInt - 3]}"
                    }
                    localIOException3++
                }
                localPrintWriter.flush()
                localPrintWriter.close()
                return "tc.Memory dumped."
            }
        })
        this.commands.put("d", this.commands.get("dump"))


        this.commands.put("set", object : Command {
            override val usage: String
                get() = "set [ PC | reg | PSR | MPR | mem_addr | label ] [ mem_addr | label ] [ value | N | Z | P ]"

            override val help: String
                get() =
                    "Sets the value of a register/PC/PSR/label/memory location/memory range or set the condition codes individually."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt < 2 || paramInt > 4) {
                    return usage
                }
                if (paramInt == 2) {
                    return setConditionCodes(paramArrayOfString[1]) ?: return usage
                }
                var i = Word.parseNum(paramArrayOfString[paramInt - 1])
                if (i == Int.MAX_VALUE) {
                    i = this@CommandLine.mac.lookupSym(paramArrayOfString[paramInt - 1])
                }
                if (i == Int.MAX_VALUE) {
                    return "Error: Invalid value (" + paramArrayOfString[paramInt - 1] + ")"
                }
                if (paramInt == 3) {
                    val str2 = this@CommandLine.setRegister(paramArrayOfString[1], i)
                    if (str2 != null) {
                        return str2
                    }
                }

                val j = this@CommandLine.mac.getAddress(paramArrayOfString[1])

                if (j == Int.MAX_VALUE) {
                    return "Error: Invalid register, address, or label  ('${paramArrayOfString[1]}')"
                }
                if (j < 0 || j >= 65535) {
                    return "Address ${paramArrayOfString[1]} out of bounds"
                }
                val k: Int
                if (paramInt == 3) {
                    k = j
                } else {
                    k = this@CommandLine.mac.getAddress(paramArrayOfString[2])
                    if (k == Int.MAX_VALUE) {
                        return "Error: Invalid register, address, or label  ('${paramArrayOfString[1]}')"
                    }
                    if (k < 0 || k >= 65535) {
                        return "Address ${paramArrayOfString[2]} out of bounds"
                    }
                    if (k < j) {
                        return "Second address in range (${paramArrayOfString[2]}) must be >= first (${paramArrayOfString[1]})"
                    }
                }
                for (m in j..k) {
                    this@CommandLine.mac.writeMemory(m, i)
                }

                return if (paramInt == 3) {
                    "tc.Memory location ${Word.toHex(j)} updated to ${paramArrayOfString[paramInt - 1]}"
                } else "tc.Memory locations ${Word.toHex(k)} to ${Word.toHex(k)} updated to ${paramArrayOfString[paramInt - 1]}"
            }
        })

        this.commands.put("list", object : Command {
            override val usage: String
                get() = "l[ist] [ addr1 | label1 [addr2 | label2] ]"

            override val help: String
                get() = "Lists the contents of memory locations (default address is PC. Specify range by giving 2 arguments)."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt > 3) {
                    return usage
                }
                if (paramInt == 1) {
                    this@CommandLine.scrollToPC()
                    return "${Word.toHex(this@CommandLine.mac.pc)} : ${this@CommandLine.mac.currentInst.toHex()} : ${Instruction.disassemble(this@CommandLine.mac.currentInst, this@CommandLine.mac.pc, this@CommandLine.mac)}"
                }

                if (paramInt == 2) {
                    val str1 = this@CommandLine.getRegister(paramArrayOfString[1])
                    if (str1 != null) {
                        return "${paramArrayOfString[1]} : $str1"
                    }

                    // TODO: check if 'int j' should be global instead of local.
                    val j = this@CommandLine.mac.getAddress(paramArrayOfString[1])
                    if (j == Int.MAX_VALUE) {
                        return "Error: Invalid address or label (" + paramArrayOfString[1] + ")"
                    }
                    if (TCSimulator.GRAPHICAL_MODE && j < 65024) {
                        this@CommandLine.GUI.scrollToIndex(j)
                    }
                    return "${Word.toHex(j)} : ${this@CommandLine.mac.readMemory(j).toHex()} : ${Instruction.disassemble(this@CommandLine.mac.readMemory(j), j, this@CommandLine.mac)}"
                }

                val i = this@CommandLine.mac.getAddress(paramArrayOfString[1])
                val j = this@CommandLine.mac.getAddress(paramArrayOfString[2])
                if (i == Int.MAX_VALUE)
                    return "Error: Invalid address or label (${paramArrayOfString[1]})"
                if (j == Int.MAX_VALUE)
                    return "Error: Invalid address or label (${paramArrayOfString[2]})"
                if (j < i) {
                    return "Error: addr2 should be larger than addr1"
                }
                var str2 = ""
                for (k in i..j) {
                    str2 = str2 + "${Word.toHex(k)} : ${this@CommandLine.mac.readMemory(k).toHex()} : ${Instruction.disassemble(this@CommandLine.mac.readMemory(k), k, this@CommandLine.mac)}"
                    if (k != j)
                        str2 = str2 + "\n"
                }
                if (TCSimulator.GRAPHICAL_MODE) {
                    this@CommandLine.GUI.scrollToIndex(i)
                }
                return str2
            }
        })
        this.commands.put("l", this.commands["list"])

        // TODO: Change all 'as' to 'asm'
        this.commands.put("as", object : Command {
            override val usage: String
                get() = "as [-warn] <filename>"

            override val help: String
                get() = "Assembles <filename> showing errors and (optionally) warnings, and leaves a .obj file in the same directory."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (paramInt < 2 || paramInt > 3) {
                    return usage
                }
                val arrayOfString = arrayOfNulls<String>(paramInt - 1)
                var str1 = ""
                arrayOfString[0] = paramArrayOfString[1]
                str1 = str1 + paramArrayOfString[1]

                if (paramInt == 3) {
                    arrayOfString[1] = paramArrayOfString[2]
                    str1 = str1 + " " + paramArrayOfString[2]
                }
                val localLC3as = TCasm()
                var str2 = ""

                try {
                    str2 = localLC3as.asm(arrayOfString)
                    if (str2.isNotEmpty()) {
                        return str2 + "Warnings encountered during assembly " + "(but assembly completed w/o errors)."
                    }
                } catch (localAsmException: com.tc.AsmException) {
                    return localAsmException.message + "Errors encountered during assembly."
                }

                return "Assembly of '$str1' completed without errors or warnings."
            }
        })

        this.commands.put("clear", object : Command {
            override val usage: String
                get() = "clear"

            override val help: String
                get() = "Clears the commandline output window. Only available in GUI mode."

            override fun doCommand(paramArrayOfString: Array<String>, paramInt: Int): String {
                if (TCSimulator.GRAPHICAL_MODE) {
                    this@CommandLine.GUI.clearCommandLine()
                    return ""
                }
                return "Error: clear is only available in GUI mode"
            }
        })
    }

    fun setGUI(paramTC3GUI: TC3GUI) {
        this.GUI = paramTC3GUI
    }

    @Throws(CustomException::class, NumberFormatException::class)
    fun runCommand(paramString: String?): String {
        var paramString1: String? = if (paramString != null) paramString else return ""

        if (!paramString1!!.startsWith("@")) {
            resetHistoryStack()
            addToHistory(paramString1)
        } else {
            paramString1 = paramString1.replaceFirst("^@".toRegex(), "")
        }
        //        Object localObject1 = paramString.split("\\s+");
        var localObject1 = paramString1.split("\\s+".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var i = localObject1.size

        if (i == 0) {
            return ""
        }
        val str = localObject1[0].toLowerCase()

        if (str == "") {
            return ""
        }

        var j = -1
        for (k in localObject1.indices) {
            if (localObject1[k].startsWith("#")) {
                j = k
                break
            }
        }

        if (j == 0) {
            return ""
        }

        if (j >= 0) {
//                localObject2 = new String[j];
//            val localObject2 = arrayOfString<String>(j)
            val localObject2 = arrayOfNulls<String>(j)
            for (m in 0 until j) {
                localObject2[m] = localObject1[m]
            }
            localObject1 = localObject2.requireNoNulls()
            i = localObject1.size
        }

        val localObject2 = this.commands[str] ?: return "Unknown command: $str"
        return localObject2.doCommand(localObject1, i)
    }

    fun scrollToPC() {
        if (TCSimulator.GRAPHICAL_MODE) {
            this.GUI.scrollToPC()
        }
    }

    fun setRegister(paramString: String, paramInt: Int): String? {
        var str: String? = "Register " + paramString.toUpperCase() + " updated to value " + Word.toHex(paramInt)

        if (paramString.equals("pc", ignoreCase = true)) {
            this.mac.pc = paramInt
            scrollToPC()
        } else if (paramString.equals("psr", ignoreCase = true)) {
            this.mac.psr = paramInt
        } else if (paramString.equals("mpr", ignoreCase = true)) {
            this.mac.memory
            this.mac.memory.write(65042, paramInt)
        } else if ((paramString.startsWith("r") || paramString.startsWith("R")) && paramString.length == 2) {
            val localInt = paramString.substring(1, 2).toInt()
            this.mac.setRegister(localInt, paramInt)
        } else {
            str = null
        }
        return str
    }

    fun setConditionCodes(paramString: String): String? {
        var str: String? = null
        if (paramString.equals("n", ignoreCase = true)) {
            this.mac.setN()
            str = "PSR N bit set"
        } else if (paramString.equals("z", ignoreCase = true)) {
            this.mac.setZ()
            str = "PSR Z bit set"
        } else if (paramString.equals("p", ignoreCase = true)) {
            this.mac.setP()
            str = "PSR P bit set"
        }
        return str
    }

    fun getRegister(paramString: String): String? {
        val i: Int

        if (paramString.equals("pc", ignoreCase = true)) {
            i = this.mac.pc
        } else if (paramString.equals("psr", ignoreCase = true)) {
            i = this.mac.psr
        } else if (paramString.equals("mpr", ignoreCase = true)) {
            i = this.mac.mpr
        } else if ((paramString.startsWith("r") || paramString.startsWith("R")) && paramString.length == 2) {
            val localInt = (paramString.substring(1, 2)).toInt()
            i = this.mac.getRegister(localInt)
        } else {
            return null
        }
        return Word.toHex(i)
    }

    fun checkRegister(paramString: String, paramInt: Int): Boolean? {
        val i = Word.parseNum(this.getRegister(paramString)!!)
        if (i == Int.MAX_VALUE) {
            return null
        }
        val localWord = Word(paramInt)
        return i == localWord.getValue()
    }

//    companion object {
//        val PROMPT = "\n==>"
//    }

    //    private interface main.java.com.rainwire.tc3.tc.Command {
    //        String usage;
    //
    //        String getHelp();
    //
    //        String doCommand(String[] paramArrayOfString, int paramInt) throws main.java.com.rainwire.tc3.tc.CustomException;
    //    }
}