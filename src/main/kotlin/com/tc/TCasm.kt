package com.tc

import java.io.*
import java.util.*

internal class TCasm {
    @Throws(com.tc.AsmException::class)
    fun asm(paramArrayOfString: Array<String?>): String {
        val str1 = "Usage: tc.TCasm [-warn] file.asm"
        var str2: String? = null
        var bool = false
        val localSymTab = SymTab()

        Scanner.warnings = ""

        for (i in paramArrayOfString.indices) {
            if (paramArrayOfString[i]!!.isEmpty()) {
                error("Null arguments are not permitted.\n" + str1)
            } else if (paramArrayOfString[i]!![0] == '-') {
                if (paramArrayOfString[i].equals("-warn", ignoreCase = true)) {
                    bool = true
                } else {
                    error("Unknown flag ('" + paramArrayOfString[i] + "').\n" + str1)
                }
            } else {
                str2 = paramArrayOfString[i]
            }
        }

        if (str2 == null || str2.isEmpty()) {
            error("No .asm file specified.\n" + str1)
        }

        val str3 = baseFilename(str2)
        val localList = parsetoInstruction(str3, bool)
        passOne(localSymTab, localList)
        passTwo(localSymTab, localList)
        passThree(localList, str3)
        genSym(localSymTab, str3)

        return Scanner.warnings
    }

    @Throws(com.tc.AsmException::class)
    fun error(paramString: String) {
        throw com.tc.AsmException("ERROR: " + paramString + "\n")
    }

    @Throws(com.tc.AsmException::class)
    fun baseFilename(paramString: String?): String {
        if (!paramString!!.endsWith(".asm")) {
            error("Input file must have .asm suffix ('$paramString')")
        }
        return paramString.substring(0, paramString.length - 4)
    }

    @Throws(com.tc.AsmException::class)
    fun passOne(paramSymTab: SymTab, paramList: List<*>) {
        var i = -1
        val localIterator = paramList.iterator()

        while (localIterator.hasNext()) {
            val localInstruction = localIterator.next() as Instruction
                if (localInstruction.label.length > 20) {
                    localInstruction.scan.error("Labels can be no longer than 20 characters ('" + localInstruction.label + "').")
                }
                if (i > 65535) {
                    localInstruction.scan.error("Label cannot be represented in 16 bits ($i)")
                }
                if (!paramSymTab.insert(localInstruction.label!!, i)) {
                    localInstruction.scan.error("Duplicate label ('" + localInstruction.label + "')")
                }
            i = localInstruction.nextAddress(i)

            if (i == -2) {
                localInstruction.scan.error("tc.Instruction not preceded by a .orig directive.")
            }
        }
    }

    @Throws(com.tc.AsmException::class)
    fun passTwo(paramSymTab: SymTab, paramList: List<*>) {
        var i = -1
        val localIterator = paramList.iterator()

        while (localIterator.hasNext()) {
            val localInstruction = localIterator.next() as Instruction
            val j = paramSymTab.lookup(localInstruction.labelRef)

            if (j < 0) {
                localInstruction.scan.error("Symbol not found ('" + localInstruction.labelRef + "')")
            }
            if (localInstruction.op!!.op == 33) {
                localInstruction.offsetImmediate = j
            } else {
                localInstruction.offsetImmediate = j - (i + 1)
            }
            i = localInstruction.nextAddress(i)
        }
    }

    @Throws(com.tc.AsmException::class)
    fun passThree(paramList: List<*>, paramString: String) {
        val str = paramString + ".obj"

        try {
            val localBufferedOutputStream = BufferedOutputStream(FileOutputStream(str))
            val localIterator1 = paramList.iterator()

            while (localIterator1.hasNext()) {
                val localInstruction = localIterator1.next() as Instruction
                if (localInstruction.op.op != 37) {
                    val localList = localInstruction.encode()
                    if (localList != null) {
                        val localIterator2 = localList.iterator()
                        while (localIterator2.hasNext()) {
                            writeWordToFile(localBufferedOutputStream, (localIterator2.next() as Int).toInt())
                        }
                    }
                }
            }
            localBufferedOutputStream.close()
        } catch (localIOException: IOException) {
            error("Couldn't write file ($str)")
        }

    }

    @Throws(com.tc.AsmException::class)
    fun genSym(paramSymTab: SymTab, paramString: String) {
        val str1 = paramString + ".sym"
        val localEnumeration = paramSymTab.labels

        try {
            val localBufferedWriter = BufferedWriter(FileWriter(str1))

            localBufferedWriter.write("// Symbol table\n")
            localBufferedWriter.write("// Scope level 0:\n")
            localBufferedWriter.write("//\tSymbol Name       Page Address\n")
            localBufferedWriter.write("//\t----------------  ------------\n")

            while (localEnumeration.isNotEmpty()) {
                val str2 = localEnumeration.toString()
                localBufferedWriter.write("//\t" + str2)
                for (i in 0 until 16 - str2.length) {
                    localBufferedWriter.write(" ")
                }
                val i = paramSymTab.lookup(str2)
                val str3 = formatAddress(i)
                localBufferedWriter.write("  " + str3 + "\n")
            }
            localBufferedWriter.newLine()
            localBufferedWriter.close()
        } catch (localIOException: IOException) {
            error("Couldn't write file ($str1)")
        }

    }

    private fun formatAddress(paramInt: Int): String {
        val str = "0000" + Integer.toHexString(paramInt).toUpperCase()
        return str.substring(str.length - 4)
    }

    @Throws(com.tc.AsmException::class)
    fun parsetoInstruction(paramString: String, paramBoolean: Boolean): List<Instruction> {
        val str1 = paramString + ".asm"
        val localArrayList = ArrayList<Instruction>()
        var i = 1

        try {
            val localScanner = java.util.Scanner(File(str1))
            while (localScanner.hasNext()) {
                val localInstruction = Instruction(localScanner.next(), i++, paramBoolean)
                if (localInstruction.valid) {
                    localArrayList.add(localInstruction)
                }
            }
            localScanner.close()
        } catch (localIOException: IOException) {
            error("Couldn't read file ($str1)")
        }

        return localArrayList
    }

    @Throws(IOException::class)
    fun writeWordToFile(paramBufferedOutputStream: BufferedOutputStream, paramInt: Int) {
        val i = (paramInt shr 8 and 0xFF).toByte().toInt()
        val j = (paramInt and 0xFF).toByte().toInt()
        paramBufferedOutputStream.write(i)
        paramBufferedOutputStream.write(j)
    }
}
