package com.tc

class Instruction {
    var valid = false
    var opcode = -1
    lateinit var op: Ops
    var label: String = ""
    var reg = intArrayOf(-1, -1, -1)
    var offsetImmediate = 0
    var labelRef: String = ""
    lateinit var stringz: String
    var n = false
    var z = false
    var p = false
    lateinit var scan: Scanner

    constructor() {
        this.valid = false
    }

    @Throws(com.tc.AsmException::class)
    constructor(paramString: String, paramInt: Int, paramBoolean: Boolean) {
        this.valid = false
        this.scan = Scanner(paramString, paramInt, paramBoolean)
        build()
        this.scan.scanning = false
    }

    @Throws(com.tc.AsmException::class)
    fun build() {
        var str1 = this.scan.remainingLine
        if (str1.isEmpty())
            return
        if (str1.get(0) == ';')
            return
        this.valid = true

        val str2 = this.scan.get_lexeme_lab(false)
        var localOps = Scanner.getOpcodeFromLexeme(str2)
        if (str2.isNotEmpty()) {
            this.label = str2
            val bool1 = this.scan.eat(str2)
            if (!bool1) {
                this.scan.internalError("Eating label failed.")
            }
        }

        str1 = this.scan.remainingLine
        if (str1.isEmpty())
            return
        if (str1[0] == ';') {
            return
        }
        val str3 = this.scan.get_lexeme_lab(true)
        localOps = Scanner.getOpcodeFromLexeme(str3)
        if (localOps == null) {
            this.scan.error("Invalid opcode ('$str3')")
        }
        this.op = localOps!!
        this.opcode = localOps.opcode
        val bool2 = this.scan.eat(str3)
        if (!bool2) this.scan.internalError("Eating opcode failed.")
        updateBrFields(localOps)


        parseOperands(this.op)


        str1 = this.scan.remainingLine
        if (str1.isNotEmpty() && str1[0] != ';') {
//        if (str1 != null && str1.isNotEmpty() && str1[0] != ';') {
            this.scan.error("Junk at end of line ('$str1')")
        }
    }

    fun updateBrFields(paramOps: Ops?) {
        this.n = false
        this.z = false
        this.p = false
        when (paramOps!!.op) {
            3 -> {
                this.n = true
                this.z = true
                this.p = true
            }
            4 -> {
                this.n = true
                this.z = true
            }
            5 -> {
                this.n = true
                this.p = true
            }
            6 -> this.n = true
            7 -> {
                this.z = true
                this.p = true
            }
            8 -> this.z = true
            9 -> this.p = true
            10 -> {
                this.n = true
                this.z = true
                this.p = true
            }
        }

    }

    @Throws(com.tc.AsmException::class)
    private fun parseImmediate() {
        val str1 = this.scan.remainingLine

        if (str1.isEmpty()) {
            this.scan.error("Immediate expected.")
        }

        val str2 = this.scan.lexemeImmediate
        if (str2.isEmpty()) {
            this.scan.error("Invalid immediate; perhaps you need to include a '#', 'x', or 'b' prefix?")
        }

        val bool = this.scan.eat(str2)
        if (!bool) this.scan.internalError("Eating immediate failed.")
        this.offsetImmediate = this.scan.imm_val(str2)
    }

    @Throws(com.tc.AsmException::class)
    fun parseRegImm (paramInt: Int, paramBoolean: Boolean): String {
        val str1 = this.scan.remainingLine

        if (str1.isEmpty()) {
            this.scan.error("Register ${(if (paramBoolean) "or immediate" else "")}expected at operand ${(paramInt + 1)}")
        }


        val str2 = this.scan.lexeme_reg
        if (str2.isEmpty()) {
            if (paramBoolean) {
                parseImmediate()
            } else {
                this.scan.error("Register expected for operand ${(paramInt + 1)}")
            }
        } else {
            this.reg[paramInt] = Scanner.regNum(str2)
            if (!regesterLegal(this.reg[paramInt])) {
                this.scan.error("Illegal register ('$str2')")
            }
            val bool = this.scan.eat(str2)
            if (!bool) this.scan.internalError("Eating register failed.")
        }
        return str1
    }

    @Throws(com.tc.AsmException::class)
    fun parseString() {
        val str = this.scan.string
        if (str == null) {
            this.scan.error("Bad string")
        }

        val bool = this.scan.eat(str)
        if (!bool) {
            this.scan.internalError("Eating string failed.")
        }

        this.stringz = this.scan.get_real_string(str)!!
    }

    @Throws(com.tc.AsmException::class)
    fun parseLabelOffset() {
        val str1 = this.scan.remainingLine
        var str2 = this.scan.get_lexeme_lab(false)
        val bool: Boolean
        if (str2.isEmpty() || str2.isNotEmpty() && (str2[0] == 'x' || str2[0] == 'b')) {
            if (str2.isNotEmpty()) {
                this.scan.warning("Operand begins with 'x' or 'b'; assuming it's an immediate offset (i.e., not a label)")
            }

            str2 = this.scan.lexemeImmediate
            if (str2.isEmpty()) {
                this.scan.error("Bad immediate value.")
            }
            bool = this.scan.eat(str2)
            if (!bool) this.scan.internalError("Eating immediate failed.")
            this.offsetImmediate = this.scan.imm_val(str2)
        } else {
            bool = this.scan.eat(str2)
            if (!bool) this.scan.internalError("Eating label failed.")
            this.labelRef = str2
        }
    }

    @Throws(com.tc.AsmException::class)
    fun parseOperands(paramOps: Ops?) {
        this.scan.remainingLine
        var bool: Boolean
        when (paramOps!!.op) {
            0, 1, 2, 21 -> {
                parseRegImm(0, false)
                bool = this.scan.eat(",")
                if (!bool) this.scan.error("Comma expected after operand 1")
                parseRegImm(1, false)
                if (paramOps.op != 21) {
                    bool = this.scan.eat(",")
                    if (!bool) this.scan.error("Comma expected after operand 2")
                    parseRegImm(2, true)
                }
            }
            3, 4, 5, 6, 7, 8, 9, 10 -> parseLabelOffset()


            11, 14, 15, 16, 17 -> {
                parseRegImm(0, false)
                bool = this.scan.eat(",")
                if (!bool) {
                    this.scan.error("Comma expected between operands")
                }

                parseLabelOffset()
            }


            12, 13 -> {
                parseRegImm(0, false)
                bool = this.scan.eat(",")
                if (!bool) {
                    this.scan.error("Comma expected after operand 1")
                }
                parseRegImm(1, false)
                bool = this.scan.eat(",")
                if (!bool) {
                    this.scan.error("Comma expected after operand 2")
                }

                parseLabelOffset()
            }

            18, 37 -> {

            }


            19, 24, 25 -> parseRegImm(0, false)


            20, 33 -> parseLabelOffset()


            36 -> parseString()
            22, 23 -> this.reg[0] = 7

            27 -> this.offsetImmediate = 32
            28 -> this.offsetImmediate = 33
            29 -> this.offsetImmediate = 34
            30 -> this.offsetImmediate = 35
            31 -> this.offsetImmediate = 36
            32 -> this.offsetImmediate = 37


            26, 34, 35 -> parseImmediate()
        }

    }

    fun nextAddress(paramInt: Int): Int {
            if (paramInt == -1 && this.op!!.op != 35) {
                return -2
            }
            when (this.op.op) {
                35 -> return this.offsetImmediate
                34 -> return paramInt + this.offsetImmediate
                36 -> return paramInt + this.stringz.length + 1
            }
            return paramInt + 1
    }

    @Throws(com.tc.AsmException::class)
    fun encode(): List<Int> {
        val localArrayList = ArrayList<Int>()
        var i = 0

//        if (this.op == null) return null
        if (this.op.opcode != -1) {
            i = addField(i, this.op.opcode, 15, 12)
        }
        var j: Int
        when (this.op.op) {
            0, 1, 2, 21 -> {
                i = addField(i, this.reg[0], 11, 9)
                i = addField(i, this.reg[1], 8, 6)
                if (this.op.op != 21) {
                    if (this.reg[2] == -1) {
                        checkRange(this.offsetImmediate, -16, 15)
                        i = addField(i, 1, 5, 5)
                        i = addField(i, this.offsetImmediate, 4, 0)
                    } else {
                        i = addField(i, this.reg[2], 2, 0)
                    }
                } else {
                    i = addField(i, 63, 5, 0)
                }
            }
            3, 4, 5, 6, 7, 8, 9, 10 -> {
                checkRange(this.offsetImmediate, 65280, 255)
                i = addField(i, if (this.n) 1 else 0, 11, 11)
                i = addField(i, if (this.z) 1 else 0, 10, 10)
                i = addField(i, if (this.p) 1 else 0, 9, 9)
                i = addField(i, this.offsetImmediate, 8, 0)
            }
            11, 14, 15, 16, 17 -> {
                checkRange(this.offsetImmediate, 65280, 255)
                i = addField(i, this.reg[0], 11, 9)
                i = addField(i, this.offsetImmediate, 8, 0)
            }
            12, 13 -> {
                checkRange(this.offsetImmediate, -32, 31)
                i = addField(i, this.reg[0], 11, 9)
                i = addField(i, this.reg[1], 8, 6)
                i = addField(i, this.offsetImmediate, 5, 0)
            }
            18 -> {
            }

            37 -> this.scan.internalError("Trying to generate instruction for '.end'.")

            19, 24 -> i = addField(i, this.reg[0], 8, 6)

            25 -> {
                i = addField(i, this.reg[0], 8, 6)
                i = addField(i, 1, 0, 0)
            }

            20 -> {
                checkRange(this.offsetImmediate, 64512, 1023)
                i = addField(i, 1, 11, 11)
                i = addField(i, this.offsetImmediate, 10, 0)
            }
            33 -> {
                checkRange(this.offsetImmediate, 32768, 65535)
                i = this.offsetImmediate
            }

            22 -> i = addField(i, 7, 8, 6)

            23 -> {
                i = addField(i, 7, 8, 6)
                i = addField(i, 1, 0, 0)
            }

            26, 27, 28, 29, 30, 31, 32 -> {
                checkRange(this.offsetImmediate, 0, 255)
                i = addField(i, this.offsetImmediate, 7, 0)
            }
            35 -> {
                checkRange(this.offsetImmediate, 0, 65535)
                i = this.offsetImmediate
            }
            34 -> {
                checkRange(this.offsetImmediate, 1, 65535)
                j = 0
                while (j < this.offsetImmediate) {
                    localArrayList.add(0)
                    j++
                }
                return localArrayList
            }
            36 -> {
                j = 0
                while (j < this.stringz.length) {
                    // TODO: Make sure 'this.stringz[j]' is actually supposed to be turned into an Int
                    localArrayList.add(this.stringz[j].toInt())
                    j++
                }

                localArrayList.add(0)
                return localArrayList
            }
        }

        localArrayList.add(i)

        return localArrayList
    }

    @Throws(com.tc.AsmException::class)
    fun checkRange(paramInt1: Int, paramInt2: Int, paramInt3: Int) {
        if (paramInt1 < paramInt2 || paramInt1 > paramInt3) {
            this.scan.error("Immediate operand (#$paramInt1) out of range (#$paramInt2 to #$paramInt3)")
        }
    }

    fun printLabelOffset() {
        if (this.labelRef != null) {
            println(this.labelRef)
        } else {
            println("#" + this.offsetImmediate)
        }
    }

    fun print() {
        if (this.label != null) {
            print(this.label!! + "\t")
        } else {
            print("\t")
        }

        if (this.op == null) {
            return
        }
        print(this.op!!.opstring + "\t")

        when (this.op!!.op) {
            0, 1, 2, 21 -> {
                print("R" + this.reg[0] + ",R" + this.reg[1])
                if (this.op!!.op != 21) {
                    if (this.reg[2] != -1) {
                        println(",R" + this.reg[2])
                    } else {
                        println(",#" + this.offsetImmediate)
                    }
                }
            }
            3, 4, 5, 6, 7, 8, 9, 10 -> printLabelOffset()
            11, 14, 15, 16, 17 -> {
                print("R" + this.reg[0] + ",")
                printLabelOffset()
            }
            12, 13 -> {
                print("R" + this.reg[0] + "," + "R" + this.reg[1] + ",")
                printLabelOffset()
            }

            18, 37 -> println()

            19, 24, 25 -> print("R" + this.reg[0])

            20, 33 -> printLabelOffset()

            22, 23 -> {
            }

            27, 28, 29, 30, 31, 32 -> {
            }

            26, 34, 35 -> print("#" + this.offsetImmediate)
        }

    }

    companion object {
        val NO_OP = 0
        val BR_OP = 0
        val ADD_OP = 1
        val LD_OP = 2
        val ST_OP = 3
        val JSRR_OP = 4
        val JSR_OP = 4
        val AND_OP = 5
        val LDR_OP = 6
        val STR_OP = 7
        val RTI_OP = 8
        val NOT_OP = 9
        val LDI_OP = 10
        val STI_OP = 11
        val RET_OP = 12
        val JMP_OP = 12
        val MUL_OP = 13
        val LEA_OP = 14
        val TRAP_OP = 15
        private val rnames = arrayOf("R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7")

        fun regesterLegal(paramInt: Int): Boolean {
            return paramInt in 0..7
        }

        fun addField(paramInt1: Int, paramInt2: Int, paramInt3: Int, paramInt4: Int): Int {
            if (paramInt4 > paramInt3) {
                return addField(paramInt1, paramInt2, paramInt4, paramInt3)
            }

            var i = -1
            i = i shl paramInt3 - paramInt4 + 1
            i = i xor 0xFFFFFFFF.toInt()
            i = i shl paramInt4

            return i and (paramInt2 shl paramInt4) or (i xor 0xFFFFFFFF.toInt() and paramInt1)
        }

        @Throws(CustomException::class)
        fun execute(paramWord: Word, paramMachine: Machine) {
            val i3 = paramWord.getZext(15, 12)
            val i: Int
            i = paramWord.getZext(8, 6)
            val j = paramWord.getZext(2, 0)
            val k = paramWord.getZext(11, 9)
            val m = paramWord.getSext(4, 0)
            val i1 = paramWord.getSext(8, 0)
            val i2 = paramWord.getSext(10, 0)
            val i5 = paramWord.getSext(5, 0)
            val i6 = paramWord.getZext(7, 0)

            val localRegisterFile = paramMachine.registerFile
            val localMemory = paramMachine.memory
            var i7 = paramMachine.pc + 1
            val i8: Int
            var i9: Int
            val i10: Int
            val i11: Int
            when (i3) {
                1 -> {
                    if (paramWord.getBit(5) == 0) {
                        if (paramWord.getZext(4, 3) == 0) {
                            i8 = localRegisterFile.getRegister(i) + localRegisterFile.getRegister(j)
                            localRegisterFile.write(k, i8)
                        } else {
                            throw IllegalInstructionException("ADD: Bits 4 and 3 are not zero")
                        }
                    } else {
                        i8 = localRegisterFile.getRegister(i) + m
                        localRegisterFile.write(k, i8)
                    }
                    localRegisterFile.setNZP(i8)
                }

                13 -> {
                    if (paramWord.getBit(5) == 0) {
                        if (paramWord.getZext(4, 3) == 0) {
                            i8 = localRegisterFile.getRegister(i) * localRegisterFile.getRegister(j)
                            localRegisterFile.write(k, i8)
                        } else {
                            throw IllegalInstructionException("ADD: Bits 4 and 3 are not zero")
                        }
                    } else {
                        i8 = localRegisterFile.getRegister(i) * m
                        localRegisterFile.write(k, i8)
                    }
                    localRegisterFile.setNZP(i8)
                }

                5 -> {
                    if (paramWord.getBit(5) == 0) {
                        if (paramWord.getZext(4, 3) == 0) {
                            i8 = localRegisterFile.getRegister(i) and localRegisterFile.getRegister(j)
                            localRegisterFile.write(k, i8)
                        } else {
                            throw IllegalInstructionException("AND: Bits 4 and 3 are not zero")
                        }
                    } else {
                        i8 = localRegisterFile.getRegister(i) and m
                        localRegisterFile.write(k, i8)
                    }
                    localRegisterFile.setNZP(i8)
                }

                0 -> {
                    i9 = if (paramWord.getBit(11) == 1) 1 else 0
                    i10 = if (paramWord.getBit(10) == 1) 1 else 0
                    i11 = if (paramWord.getBit(9) == 1) 1 else 0


                    val bool1 = localRegisterFile.n
                    val bool2 = localRegisterFile.z
                    val bool3 = localRegisterFile.p
                    if (i9 == 0 && i10 == 0 && i11 == 0)
                        throw IllegalInstructionException("BR: At least one of n/z/p has to be set")
                    if (i9 != 0 && bool1 || i10 != 0 && bool2 || i11 != 0 && bool3) {
                        i7 += i1
                    }
                }
                4 -> {
                    i9 = localRegisterFile.getRegister(i)
                    localRegisterFile.write(7, i7)
                    if (paramWord.getBit(11) == 0) {
                        if (paramWord.getZext(5, 0) != 0) {
                            throw IllegalInstructionException("JSRR: Bits[5-0] should be zero")
                        }
                        i7 = i9
                    } else {
                        i7 += i2
                    }
                }

                2 -> {
                    i9 = i7 + i1
                    paramMachine.checkAddr(i9)
                    i8 = localMemory.read(i9)?.getValue()!!
                    localRegisterFile.write(k, i8)
                    localRegisterFile.setNZP(i8)
                }

                10 -> {
                    i9 = i7 + i1
                    paramMachine.checkAddr(i9)
                    i9 = localMemory.read(i9)?.getValue()!!
                    paramMachine.checkAddr(i9)
                    i8 = localMemory.read(i9)?.getValue()!!
                    localRegisterFile.write(k, i8)
                    localRegisterFile.setNZP(i8)
                }

                6 -> {
                    i9 = localRegisterFile.getRegister(i) + i5
                    paramMachine.checkAddr(i9)
                    i8 = localMemory.read(i9)?.getValue()!!
                    localRegisterFile.write(k, i8)
                    localRegisterFile.setNZP(i8)
                }

                14 -> {
                    i8 = i7 + i1
                    localRegisterFile.write(k, i8)
                    localRegisterFile.setNZP(i8)
                }

                9 -> {
                    if (paramWord.getZext(5, 0) != 63) {
                        throw IllegalInstructionException("NOT: Bits[5-0] should be all 1s")
                    }
                    i8 = localRegisterFile.getRegister(i) xor 0xFFFFFFFF.toInt()
                    localRegisterFile.write(k, i8)
                    localRegisterFile.setNZP(i8)
                }

                12 -> {
                    if (paramWord.getZext(11, 9) != 0)
                        throw IllegalInstructionException("JMP: Bits[11-9] should be all 0s")
                    if (i != 7 && i5 != 0)
                        throw IllegalInstructionException("JMP: Bits[5-0] should be all 0s")
                    if (i == 7 && i5 != 0 && i5 != 1) {
                        throw IllegalInstructionException("RET/RTT: Bits[5-0] are invalid")
                    }
                    if (paramWord.getZext(0, 0) == 1) {
                        localRegisterFile.privMode = false
                    }
                    i7 = localRegisterFile.getRegister(i)
                }

                8 -> {
                    if (paramWord.getZext(11, 0) != 0) {
                        throw IllegalInstructionException("RTI: Bits[11-0] should be all 0s")
                    }
                    if (localRegisterFile.privMode) {
                        i9 = localRegisterFile.getRegister(6)
                        i10 = localMemory.read(i9)!!.getValue()
                        i7 = i10
                        localRegisterFile.write(6, i9 + 1)
                        i11 = localMemory.read(i9)!!.getValue()
                        localRegisterFile.psr = i11
                    } else {
                        throw IllegalInstructionException("RTI can only be executed in privileged mode")
                    }
                }
                3 -> {
                    i9 = i7 + i1
                    paramMachine.checkAddr(i9)
                    localMemory.write(i9, localRegisterFile.getRegister(k))
                }

                11 -> {
                    i9 = i7 + i1
                    paramMachine.checkAddr(i9)
                    i9 = localMemory.read(i9)!!.getValue()
                    paramMachine.checkAddr(i9)
                    localMemory.write(i9, localRegisterFile.getRegister(k))
                }

                7 -> {
                    i9 = localRegisterFile.getRegister(i) + i5
                    paramMachine.checkAddr(i9)
                    localMemory.write(i9, localRegisterFile.getRegister(k))
                }

                15 -> {
                    if (paramWord.getZext(11, 8) != 0) {
                        throw IllegalInstructionException("TRAP: Bits[11-8] should be all 0s")
                    }

                    localRegisterFile.privMode = true


                    localRegisterFile.write(7, i7)

                    i7 = localMemory.read(i6)!!.getValue()
                }

                else -> {
                    println("tc.Instruction.execute: got default case")
                    throw IllegalInstructionException("Bad opcode ($i3)")
                }
            }


            paramMachine.pc = i7
        }

        fun printFill(paramWord: Word): String {
            return ".FILL " + paramWord.toHex()
        }

        private fun get_addr(paramInt1: Int, paramInt2: Int, paramMachine: Machine): String {
            val i = paramInt1 + paramInt2 + 1
            val str = paramMachine.lookupSym(i)

            return str ?: Word.toHex(i).toString()
        }

        fun disassemble(paramWord: Word, paramInt: Int, paramMachine: Machine): String? {
            val i3 = paramWord.getZext(15, 12)
            val i = paramWord.getZext(8, 6)
            val j = paramWord.getZext(2, 0)
            val k = paramWord.getZext(11, 9)
            val m = paramWord.getSext(4, 0)
            val i1 = paramWord.getSext(8, 0)
            val i2 = paramWord.getSext(10, 0)
            val i5 = paramWord.getSext(5, 0)
            val i6 = paramWord.getZext(7, 0)
            var str1: String? = null
            val str2 = " "
            val str3 = "#"
            val str4 = ", "
            val i7: Int
            when (i3) {
                1 -> str1 = if (paramWord.getBit(5) == 0) {
                    if (paramWord.getZext(4, 3) == 0) {
                        "ADD${str2 + rnames[k] + str4 + rnames[i] + str4 + rnames[j]}"
                    } else
                        printFill(paramWord)
                } else {
                    "ADD${str2 + rnames[k] + str4 + rnames[i] + str4 + str3 + m}"
                }

                13 -> str1 = if (paramWord.getBit(5) == 0) {
                    if (paramWord.getZext(4, 3) == 0) {
                        "MUL${str2 + rnames[k] + str4 + rnames[i] + str4 + rnames[j]}"
                    } else
                        printFill(paramWord)
                } else {
                    "MUL${str2 + rnames[k] + str4 + rnames[i] + str4 + str3 + m}"
                }

                5 -> str1 = if (paramWord.getBit(5) == 0) {
                    if (paramWord.getZext(4, 3) == 0) {
                        "AND${str2 + rnames[k] + str4 + rnames[i] + str4 + rnames[j]}"
                    } else
                        printFill(paramWord)
                } else {
                    "AND${str2 + rnames[k] + str4 + rnames[i] + str4 + str3 + m}"
                }

                0 -> {
                    i7 = if (paramWord.getBit(11) == 1) 1 else 0
                    val i8 = if (paramWord.getBit(10) == 1) 1 else 0
                    val i9 = if (paramWord.getBit(9) == 1) 1 else 0

                    if (i7 != 0 && i8 != 0 && i9 != 0) {
                        str1 = "BR${str2 + get_addr(paramInt, i1, paramMachine)}"
                    } else if (i7 != 0 || i8 != 0 || i9 != 0) {
                        str1 = "BR"
                        if (i7 != 0)
                            str1 = str1 + "n"
                        if (i8 != 0)
                            str1 = str1 + "z"
                        if (i9 != 0)
                            str1 = str1 + "p"
                        str1 = str1 + str2 + get_addr(paramInt, i1, paramMachine)
                    } else {
                        str1 = printFill(paramWord)
                    }
                }

                12 -> str1 = if (k == 0 && i5 == 0) {
                    if (i == 7) {
                        "RET"
                    } else {
                        "JMP${str2 + rnames[i]}"
                    }
                } else if (k == 0 && i5 == 1) {
                    "RTT"
                } else {
                    printFill(paramWord)
                }

                4 -> str1 = if (paramWord.getBit(11) == 0) {
                    if (paramWord.getZext(10, 9) == 0 && i5 == 0) {
                        "JSRR${str2 + rnames[i]}"
                    } else
                        printFill(paramWord)
                } else {
                    "JSR${str2 + get_addr(paramInt, i2, paramMachine)}"
                }

                2 -> str1 = "LD${str2 + rnames[k] + str4 + get_addr(paramInt, i1, paramMachine)}"

                10 -> str1 = "LDI${str2 + rnames[k] + str4 + get_addr(paramInt, i1, paramMachine)}"

                6 -> str1 = "LDR${str2 + rnames[k] + str4 + rnames[i] + str4 + i5}"

                14 -> str1 = "LEA${str2 + rnames[k] + str4 + get_addr(paramInt, i1, paramMachine)}"

                9 -> {
                    i7 = paramWord.getZext(5, 0)
                    str1 = if (i7 == 63) {
                        "NOT${str2 + rnames[k] + str4 + rnames[i]}"
                    } else
                        printFill(paramWord)
                }

                8 -> if (i2 == 0) {
                    str1 = "RTI"
                } else
                    str1 = printFill(paramWord)

                3 -> str1 = "ST${str2 + rnames[k] + str4 + get_addr(paramInt, i1, paramMachine)}"

                11 -> str1 = "STI${str2 + rnames[k] + str4 + get_addr(paramInt, i1, paramMachine)}"

                7 -> str1 = "STR${str2 + rnames[k] + str4 + rnames[i] + str4 + str3 + i5}"

                15 -> {
                    i7 = paramWord.getZext(11, 8)
                    if (i7 == 0) {
                        str1 = "TRAP${str2 + str3 + i6}"
                    } else {
                        str1 = printFill(paramWord)
                    }
                }
            }
            return str1
        }

        fun nextOver(paramWord: Word): Boolean {
            val i = paramWord.getZext(15, 12)

            return i == 4 || i == 4 || i == 15
        }
    }
}
