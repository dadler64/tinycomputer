package com.tc

class RegisterFile(private val machine: Machine) : TC3TableModel() {
    private val colNames = arrayOf("Register", "Value", "Register", "Value")
    val pc: Word
    private val MPR: Word
    private val PSR: Word
    private val MCR: Word
    private val CC: Word
    private val regArr = arrayOfNulls<Word>(8)

    init {
        for (i in 0..7)
            this.regArr[i] = Word(0)
        this.pc = Word()
        this.MPR = Word(0)
        this.MCR = Word(32768)
        this.PSR = Word(2)
        privMode.to(true)
        this.CC = this.PSR
    }

    fun reset() {
        for (i in 0..7)
            this.regArr[i]?.reset()
        this.pc.reset()

        setZ()
        privMode = true
        fireTableDataChanged()
    }

    override fun getRowCount(): Int {
        return 6
    }

    override fun getColumnCount(): Int {
        return this.colNames.size
    }

    override fun getColumnName(paramInt: Int): String {
        return this.colNames[paramInt]
    }

    override fun isCellEditable(paramInt1: Int, paramInt2: Int): Boolean {
        return paramInt2 == 1 || paramInt2 == 3
    }

    override fun getValueAt(paramInt1: Int, paramInt2: Int): Any? {
        if (paramInt2 == 0)
            return indNames[paramInt1]
        if (paramInt2 == 1)
            return this.regArr[paramInt1]?.toHex()
        if (paramInt2 == 2)
            return indNames[paramInt1 + 6]
        if (paramInt2 == 3) {
            if (paramInt1 < 2)
                return this.regArr[paramInt1 + 6]?.toHex()
            if (paramInt1 == 2)
                return this.pc.toHex()
            if (paramInt1 == 3)
                return this.MPR.toHex()
            if (paramInt1 == 4)
                return this.PSR.toHex()
            if (paramInt1 == 5)
                return printCC()
        }
        return null
    }

    override fun setValueAt(paramObject: Any?, paramInt1: Int, paramInt2: Int) {
        if (paramInt2 == 1)
            this.regArr[paramInt1]?.setValue(Word.parseNum(paramObject as String))
        else if (paramInt2 == 3) {
            if (paramInt1 < 2) {
                this.regArr[paramInt1 + 6]?.setValue(Word.parseNum(paramObject as String))
            } else {
                if (paramInt1 == 5) {
                    setNZP(paramObject as String)
                    return
                }
                if (paramObject == null && paramInt1 == 3) {
                    fireTableCellUpdated(paramInt1, paramInt2)
                    return
                }
                val i = Word.parseNum(paramObject as String)
                if (paramInt1 == 2) {
                    this.machine.pc = i
                    this.machine.scrollToPC()
                } else if (paramInt1 == 3) {
                    mpr = i
                } else if (paramInt1 == 4) {
                    psr = i
                }
            }
        }
        fireTableCellUpdated(paramInt1, paramInt2)
    }

    fun setPC(paramInt: Int) {
        this.pc.setValue(paramInt)
        fireTableCellUpdated(indRow[8], indCol[8])
    }

    fun incPC(paramInt: Int) {
        setPC(this.pc.getValue() + paramInt)
    }

    @Throws(IndexOutOfBoundsException::class)
    fun printRegister(paramInt: Int): String {
        if (paramInt < 0 || paramInt >= 8) {
            throw IndexOutOfBoundsException("Register index must be from 0 to 7")
        }
        return this.regArr[paramInt]?.toHex().toString()
    }

    @Throws(IndexOutOfBoundsException::class)
    fun getRegister(paramInt: Int): Int {
        if (paramInt < 0 || paramInt >= 8) {
            throw IndexOutOfBoundsException("Register index must be from 0 to 7")
        }
        return this.regArr[paramInt]!!.getValue()
    }

    fun write(paramInt1: Int, paramInt2: Int) {
        this.regArr[paramInt1]?.setValue(paramInt2)
        fireTableCellUpdated(indRow[paramInt1], indCol[paramInt1])
    }

    val n: Boolean
        get() = this.PSR.getBit(2) == 1

    val z: Boolean
        get() = this.PSR.getBit(1) == 1

    val p: Boolean
        get() = this.PSR.getBit(0) == 1

    var privMode: Boolean
        get() = this.PSR.getBit(15) == 1
        set(paramBoolean) {
            var i = this.PSR.getValue()
            i = if (!paramBoolean)
                i and 0x7FFF
            else
                i or 0x8000
            psr = i
        }

    fun printCC(): String {
        if (!(n xor z xor p) || n && z && p)
            return "invalid"
        if (n)
            return "N"
        if (z)
            return "Z"
        return if (p) "P" else "unset"
    }

    var psr: Int
        get() = this.PSR.getValue()
        set(paramInt) {
            this.PSR.setValue(paramInt)
            fireTableCellUpdated(indRow[10], indCol[10])
            fireTableCellUpdated(indRow[11], indCol[11])
        }

    fun setNZP(paramInt: Int) {
        var paramInt = paramInt
        var i = this.PSR.getValue()
        i = i and 0xFFFFFFF8.toInt()
        paramInt = paramInt and 0xFFFF

        if (paramInt and 0x8000 != 0)
            i = i or 0x4
        else if (paramInt == 0)
            i = i or 0x2
        else
            i = i or 0x1
        psr = i
    }

    fun setNZP(paramString: String) {
        var paramString = paramString
        paramString = paramString.toLowerCase().trim { it <= ' ' }
        if (paramString != "n" && paramString != "z" && paramString != "p") {
            CommandLinePanel.writeToConsole("Condition codes must be set as one of `n', `z' or `p'")
            return
        }
        if (paramString == "n")
            setN()
        else if (paramString == "z")
            setZ()
        else
            setP()
    }

    fun setN() {
        setNZP(32768)
    }

    fun setZ() {
        setNZP(0)
    }

    fun setP() {
        setNZP(1)
    }

    var clockMCR: Boolean
        get() = mcr and 0x8000 != 0
        set(paramBoolean) = if (paramBoolean)
            mcr = this.MCR.getValue() or 0x8000
        else
            mcr = this.MCR.getValue() and 0x7FFF

    var mcr: Int
        get() = this.MCR.getValue()
        set(paramInt) = this.MCR.setValue(paramInt)

    var mpr: Int
        get() = this.MPR.getValue()
        set(paramInt) {
            this.MPR.setValue(paramInt)
            fireTableCellUpdated(indRow[9], indCol[9])
        }

    override fun toString(): String {
        var str = "["
        for (i in 0..7)
            str = str + "R" + i + ": " + this.regArr[i]!!.toHex() + if (i != 7) "," else ""
        str = str + "]"
        str = str + "\nPC = " + this.pc.toHex()
        str = str + "\nMPR = " + this.MPR.toHex()
        str = str + "\nPSR = " + this.PSR.toHex()
        str = str + "\nCC = " + printCC()
        return str
    }

    companion object {
        private val serialVersionUID = 103L
        private val NUM_REGISTERS = 8
        private val NUM_ROWS = 12
        private val PC_ROW = 8
        private val MPR_ROW = 9
        private val PSR_ROW = 10
        private val CC_ROW = 11
        private val indNames = arrayOf("R0", "R1", "R2", "R3", "R4", "R5", "R6", "R7", "PC", "MPR", "PSR", "CC")
        private val indRow = intArrayOf(0, 1, 2, 3, 4, 5, 0, 1, 2, 3, 4, 5)
        private val indCol = intArrayOf(1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3)
    }
}