package com.tc

import java.util.*

class SymTab {
    var table: HashMap<String, Int> = HashMap()

    val labels: MutableSet<String>
        get() = this.table.keys

    /**
     * Add
     * @param paramString String value being added
     * @param paramInt Int value being added
     * @return if what you are trying to insert is already there (false) or has been added (true)
     */
    fun insert(paramString: String, paramInt: Int): Boolean {
        if (lookup(paramString) != -1) {
            return false
        }
        this.table.put(paramString, paramInt)
        return true
    }

    /**
     * @param paramString String value being looked up
     * @return Index of $paramString in 'table' or '-1' if $paramString isn't in 'table'
     */
    fun lookup(paramString: String): Int {
        return table[paramString] ?: return -1
    }
}