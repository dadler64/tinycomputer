package com.tc

open class AsmException(paramString: String) : CustomException(paramString) {
    companion object {
        private val serialVersionUID = 107L
    }
}
