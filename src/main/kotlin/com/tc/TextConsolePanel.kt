package com.tc

import java.awt.Color
import java.awt.event.*
import java.io.IOException
import java.io.PipedInputStream
import java.io.PipedOutputStream
import javax.swing.JPanel
import javax.swing.JScrollPane
import javax.swing.JTextArea
import javax.swing.text.BadLocationException

class TextConsolePanel internal constructor(kbd: KeyboardDevice, monitor: MonitorDevice) : JPanel(), KeyListener, FocusListener, ActionListener {
    private val screen: JTextArea = JTextArea(5, 21)
    private val spane: JScrollPane
    private lateinit var kbin: PipedInputStream
    private val kbout: PipedOutputStream

    init {
        this.screen.isEditable = false
        this.screen.addKeyListener(this)
        this.screen.addFocusListener(this)
        this.screen.lineWrap = true
        this.screen.wrapStyleWord = true

        this.spane = JScrollPane(this.screen, 22, 30)
        this.kbout = PipedOutputStream()
        try {
            this.kbin = PipedInputStream(this.kbout)
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

        kbd.setInputStream(this.kbin)
        kbd.setDefaultInputStream()
        kbd.setInputMode(KeyboardDevice.INTERACTIVE_MODE)
        kbd.setDefaultInputMode()
        monitor.addActionListener(this)
        add(this.spane)
    }

    override fun actionPerformed(paramActionEvent: ActionEvent) {
        val localObject1 = paramActionEvent.source
        val localObject2: Any
        if (localObject1 is Int) {
            localObject2 = this.screen.document
            try {
                localObject2.remove(0, localObject2.length)
            } catch (localBadLocationException: BadLocationException) {
                println(localBadLocationException.message)
            }

        } else {
            localObject2 = paramActionEvent.source as String
            this.screen.append(localObject2)
        }
    }

    override fun keyReleased(paramKeyEvent: KeyEvent) {

    }

    override fun keyPressed(paramKeyEvent: KeyEvent) {

    }

    override fun keyTyped(paramKeyEvent: KeyEvent) {
        val i = paramKeyEvent.keyChar.toInt()
        try {
            this.kbout.write(i)
            this.kbout.flush()
        } catch (localIOException: IOException) {
            ErrorLog.logError(localIOException)
        }

    }

    override fun focusGained(paramFocusEvent: FocusEvent) {
        this.screen.background = Color.yellow
    }

    override fun focusLost(paramFocusEvent: FocusEvent) {
        this.screen.background = Color.white
    }

    override fun setEnabled(paramBoolean: Boolean) {
        this.screen.isEnabled = paramBoolean
        if (paramBoolean) {
            this.screen.background = Color.white
        } else {
            this.screen.background = Color.lightGray
        }
    }

    companion object {
        private val serialVersionUID = 105L
    }
}