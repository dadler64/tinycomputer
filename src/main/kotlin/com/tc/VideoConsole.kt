package com.tc

import java.awt.Color
import java.awt.Dimension
import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import javax.swing.JPanel
import javax.swing.event.TableModelEvent
import javax.swing.event.TableModelListener

class VideoConsole(private val mac: Machine) : JPanel(), TableModelListener {
    private var image: BufferedImage

    init {
        val localDimension = Dimension(256, 248)
        preferredSize = localDimension
        minimumSize = localDimension
        maximumSize = localDimension
        this.image = BufferedImage(256, 248, 9)

        val localGraphics2D = this.image.createGraphics()
        localGraphics2D.color = Color.black
        localGraphics2D.fillRect(0, 0, 256, 248)
    }

    fun reset() {
        val localGraphics2D = this.image.createGraphics()
        localGraphics2D.color = Color.black
        localGraphics2D.fillRect(0, 0, 256, 248)
        repaint()
    }

    override fun tableChanged(paramTableModelEvent: TableModelEvent) {
        val i = paramTableModelEvent.firstRow
        val j = paramTableModelEvent.lastRow
        if (i == 0 && j == 65534) {
            reset()
        } else {
            if (i < 49152 || i > 65024) {
                return
            }
            val k = 2
            val m = i - 49152
            val n = m / 128 * k
            val i1 = m % 128 * k
            val i2 = this.mac.readMemory(i).convertToRGB()
            for (i3 in 0 until k) {
                for (i4 in 0 until k) {
                    this.image.setRGB(i1 + i4, n + i3, i2)
                }
            }
            repaint(i1, n, k, k)
        }
    }

    public override fun paintComponent(paramGraphics: Graphics) {
        super.paintComponent(paramGraphics)

        val localGraphics2D1 = paramGraphics as Graphics2D
        val i = width
        val j = height
        this.image = createImage(i, j) as BufferedImage
        val localGraphics2D2 = this.image.createGraphics()
        localGraphics2D2.color = Color.white
        localGraphics2D2.fillRect(0, 0, i, j)
        localGraphics2D1.drawImage(this.image, null, 0, 0)
    }

    companion object {
        private val serialVersionUID = 106L
        private val START = 49152
        private val NROWS = 128
        private val NCOLS = 124
        private val END = 65024
        private val SCALING = 2
        private val WIDTH = 256
        private val HEIGHT = 248
        private val CUR_COLOR = Color.white
    }
}