package com.tc

/** Prints the given message to the standard output stream. */
inline fun info(message: Any) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Int) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Long) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Byte) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Short) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Char) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Boolean) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Float) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: Double) {
    println("[ INFO ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun info(message: CharArray) {
    println("[ INFO ]: $message")
}

//-------------------------------------------------------------

/** Prints the given message to the standard output stream. */
inline fun debug(message: Any?) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Int) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Long) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Byte) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Short) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Char) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Boolean) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Float) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: Double) {
    println("[ DEBUG ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun debug(message: CharArray) {
    println("[ DEBUG ]: $message")
}

//-------------------------------------------------------------

/** Prints the given message to the standard output stream. */
inline fun warning(message: Any?) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Int) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Long) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Byte) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Short) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Char) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Boolean) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Float) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: Double) {
    System.err.println("[ WARNING ]: $message")
}

/** Prints the given message to the standard output stream. */
inline fun warning(message: CharArray) {
    System.err.println("[ WARNING ]: $message")
}