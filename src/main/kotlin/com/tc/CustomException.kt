package com.tc

import java.awt.Container
import javax.swing.JFrame
import javax.swing.JOptionPane

abstract class CustomException : Exception {
    constructor()

    constructor(paramString: String) : super(paramString)

    open val exceptionDescription: String
        get() = "Generic Exception: $message"

    fun showMessageDialog(paramJFrame: JFrame) {
        // [DEBUG] Show the error in the IDE console
        printerr("[tc.CustomException]: $exceptionDescription\n")
        JOptionPane.showMessageDialog(paramJFrame, exceptionDescription)
    }

    fun showMessageDialog(paramContainer: Container) {
        // [DEBUG] Show the error in the IDE console
        printerr("[tc.CustomException]: $exceptionDescription\n")
        JOptionPane.showMessageDialog(paramContainer, exceptionDescription)
    }
}